#define MAXMAT 10
#define NMS 10000
#define NBV 5000

extern "C" {
    extern double __track_mod_MOD_x;
    extern double __track_mod_MOD_y;
    extern double __track_mod_MOD_z;
    extern double __track_mod_MOD_e;
    extern double __track_mod_MOD_u;
    extern double __track_mod_MOD_v;
    extern double __track_mod_MOD_w;
    extern double __track_mod_MOD_wght;
    extern int __track_mod_MOD_kpar;
    extern int __track_mod_MOD_ibody;
    extern int __track_mod_MOD_mat;
    extern int __track_mod_MOD_ilb[5];
    extern int __track_mod_MOD_ipol;

    extern int __track_mod_MOD_mhinge;
    extern double __track_mod_MOD_e0segm;
    extern double __track_mod_MOD_desoft;
    extern double __track_mod_MOD_ssoft;

    extern double __penelope_mod_MOD_eabs[MAXMAT][3];
    extern double __penelope_mod_MOD_c1[MAXMAT];
    extern double __penelope_mod_MOD_c2[MAXMAT];
    extern double __penelope_mod_MOD_wcc[MAXMAT];
    extern double __penelope_mod_MOD_wcr[MAXMAT];

    extern double __penvared_mod_MOD_force[8][3][NBV];
    extern int __penvared_mod_MOD_ibrspl[NBV];

    struct rseed
    {
        int iseed1;
        int iseed2;
    };

    extern struct rseed rseed_;

    struct secst
    {
        double es[NMS], xs[NMS], ys[NMS], zs[NMS], us[NMS], vs[NMS], ws[NMS], wghts[NMS],
               sp1s[NMS], sp2s[NMS], sp3s[NMS], pages[NMS];
        int kps[NMS], ibodys[NMS], ms[NMS], ilbs[NMS][5], ipols[NMS];
        int nsec;  // --> amount of particles in the secondary stack.
    };

    extern struct secst secst_;

    void peinit_(double* emax,int* nmatter, int* iwr, int* info,char* pmfile);
    void cleans_(void);
    void start_(void);
    void secpar_(int* left);
    void jump_(double* ds_max,double* ds);
    void knock_(double* de,int* icol);
    void stores_(double* e, double* x, double* y, double* z,
                 double* u, double* v, double* w, double* wght,
                 int* kpar, int* ilb, int* ipol);
    double rand_(void);

    // variance reduction
    void jumpf_(double* ds_max, double* ds);
    void knockf_(double* de, int* icol);
    void vsplit_(int* nsplit);
    void vrr_(double* psurv);
    void vkill_(double* pkill);

    // positron annihilation
    void panar_(double* ecut);
}