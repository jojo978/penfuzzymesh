from setuptools import setup
from setuptools.extension import Extension
from Cython.Build import cythonize

import numpy as np

exts = [Extension("penelope.core", ["src/penelope/core.pyx"],
                  include_dirs=["external/penelope/include", np.get_include()],
                  libraries=["penelope"]),
        Extension("penelope.octree", ["src/penelope/octree.pyx"],
                  include_dirs=[np.get_include()]),
        Extension("penelope.run", ["src/penelope/run.pyx"],
                  include_dirs=["external/penelope/include", np.get_include()],
                  libraries=["penelope"]),
        Extension("penelope.triangle", ["src/penelope/triangle.pyx"],
                  include_dirs=[np.get_include()])
        ]

setup(
    name="penFuzzyMesh",
    version="0.2.0",
    packages=['penelope'],
    package_dir={'penelope': 'src/penelope'},
    ext_modules=cythonize(exts, compiler_directives={'language_level': 3}),
    package_data={'': ['*.pxd']}
)
