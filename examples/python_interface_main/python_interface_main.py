import numpy as np

import penelope as pe
from penelope.core import Par

from nb_wire_geometry import build_geometry


um = 1e-4
nm = 1e-3*um
eV = 1.0
keV = 1.0e3*eV

# all 4 materials share same eabs, wcc, wcr
pe.penelope.eabs.loc[1:4] = 1*keV
pe.penelope.wcc.loc[1:4] = 10*eV
pe.penelope.wcr.loc[1:4] = 10*eV
pe.peinit(1.0e6, ["../materials/Ti.mat",
                  "../materials/Si3N4.mat",
                  "../materials/SiO2.mat",
                  "../materials/Nb.mat"], 1)

pe.set_seed(1, 1)

# python binding to PENELOPE module variables.
track = pe.track
penvared = pe.penvared
penelope = pe.penelope

# Interaction forcing setting.
force = penvared.force

# ICOL = 5 (core ionization interaction),
# KPAR = 1 (election),
# IBODY = 1 (Ti)
# 100 x more likely Ti atom ionizations due to electrons
force.loc[5, (Par.electron, 1)] = 100

# ICOL = 4 (hard bremsstrahlung emissions),
# KPAR = 1 (electron),
# IBODY = 1 (Ti)
# force.loc[4, (Par.electron,1)] = 100

# build triangular meshes
t = build_geometry(-2000)

# mapping ibody -> ds_max
ds_maxs = {1: 7*nm, 2: 200*nm, 3: 50*nm, 5: 50*nm}

# mapping ibody to mat
body_to_mat = {1: 1, 2: 2, 3: 3, 4: 5, 5: 4}

flight_logs = dict()


def init_primary_particle():
    e = 15*keV
    x, y, z = 0, 0, 1.0e-11
    u, v, w = 0, 0, 1.0
    wght = 1.0
    kpar = 1
    pol = 0

    ilb = np.asarray([1, 0, 0, 0, 0], dtype=np.int32)

    track.ibody = 1
    track.mat = 1

    pe.stores(e, x, y, z, u, v, w, wght, kpar, ilb, pol)
    flight_logs[0] = {1}


meas = []


def run(n):
    for event in range(n):
        pe.cleans()
        init_primary_particle()

        while True:
            left = pe.secpar()

            if left < 1:
                break

            t.find_node(track.x, track.y, track.z)
            t.body_inclusion = flight_logs[pe.get_nsec()]

            # Ti emission is split into 100 photons.
            if pe.track.ilb[3] // 1000000 == 22 and track.kpar == 2:
                nsplits = 100
                if track.ilb[2] != 9:
                    track.ilb[2] = 9
                    track.wght = track.wght / nsplits

                    for _ in range(nsplits - 1):
                        aw = pe.rand() * 2.0 - 1.0
                        ar = np.sqrt(1 - aw*aw)
                        ath = pe.rand() * np.pi * 2
                        au = ar * np.cos(ath)
                        av = ar * np.sin(ath)
                        pe.stores(track.e, track.x, track.y, track.z,
                                  au, av, aw, track.wght,
                                  track.kpar,
                                  np.asarray(track.ilb, dtype=np.int32),
                                  track.ipol)
                        flight_logs[pe.get_nsec() - 1] = t.body_inclusion

            pe.start()
            ds_max = ds_maxs[track.ibody]

            while True:
                last_mat = track.mat

                ds = pe.jumpf(ds_max)
                (ncross, track.x, track.y, track.z, dsef,
                 track.ibody, track.mat) = t.step(track.x, track.y, track.z,
                                                  track.u, track.v, track.w,
                                                  ds)

                if ncross > 0 and last_mat != track.mat:
                    if track.mat == 0:
                        break

                    if track.desoft > 0 and track.kpar != 2:
                        track_e = track.e0segm - track.ssoft * dsef

                        if track_e < penelope.eabs.iloc[track.mat - 1][
                                        track.kpar - 1
                                        ]:
                            # if track.kpar == 3:
                            #     pair annihilate
                            break

                    # Check if the particle reaches the detector.
                    if track.ibody == 4:
                        if track.kpar == 2:
                            meas.append([track.e, track.x, track.y, track.z,
                                         track.wght])
                        break

                    ds_max = ds_maxs[track.ibody]

                    pe.start()
                    continue

                last_nsec = pe.get_nsec()
                de, icol = pe.knockf()

                if pe.get_nsec() != last_nsec:
                    for i_sec in range(last_nsec, pe.get_nsec()):
                        flight_logs[i_sec] = t.body_inclusion

                if track.e < penelope.eabs.iloc[track.mat - 1][track.kpar - 1]:
                    break

                # setting ds_max for the next event
                ds_max = ds_maxs[track.ibody]


for i in range(1000):
    run(20)
    print(i, pe.get_seed(), len(meas))
