import numpy as np

from penelope.mesh import box_tri_mesh
from penelope import octree


um = 1.0e-4
nm = 1.0e-3*um


def nb_wires_(offsets, body, tris):
    width = 24*um

    th1 = 70*nm
    th2 = 2*um
    th3 = 1*um
    th4 = 500*nm

    for offset in offsets:
        mins = [offset + 0*um, -width, th1 + th2]
        maxs = [offset + 2*um, +width, th1 + th2 + th4]
        box_tri_mesh(mins, maxs, tris, body)

        mins = [offset + 4*um, -width, th1 + th2 + th3 - th4]
        maxs = [offset + 6*um, +width, th1 + th2 + th3]
        box_tri_mesh(mins, maxs, tris, body)


def build_geometry(x_trans):
    width = 24*um

    th1 = 70*nm
    th2 = 2*um
    th3 = 1*um

    tris = []

    # 70 nm Ti layer (ibody=1, mat=1)
    mins = [-width, -width, 0]
    maxs = [+width, +width, th1]
    box_tri_mesh(mins, maxs, tris, 1)

    # 2 um Si3N4 layer (ibody=2, mat=2)
    mins = [-width, -width, th1]
    maxs = [+width, +width, th1 + th2]
    box_tri_mesh(mins, maxs, tris, 2)

    # 1 um SiO2 layer
    mins = [-width, -width, th1 + th2]
    maxs = [+width, +width, th1 + th2 + th3]
    box_tri_mesh(mins, maxs, tris, 3)

    # detector (ibody=4, mat=5)
    mins = [-width * 5, -width * 5, 32.0*um]
    maxs = [+width * 5, +width * 5, 32.1*um]
    box_tri_mesh(mins, maxs, tris, 4)

    # Nb wires (ibody=5, mat=4)
    nb_wires_(np.linspace(-16*um + x_trans*nm, 8*um + x_trans*nm, 4), 5, tris)

    # mapping ibody -> ds_max
    ds_maxs = {1: 7*nm, 2: 200*nm, 3: 50*nm, 5: 50*nm}

    # mapping ibody to mat
    body_to_mat = {1: 1, 2: 2, 3: 3, 4: 5, 5: 4}

    tree = octree.Octree(7, body_to_mat, ds_maxs)
    tree.set_bounds(-width * 5.01, -width * 5.01, -0.1*nm,
                    +width * 5.01, +width * 5.01, 32.2*um)
    tree.build(tris)

    return tree


def build_multi_pattern_nb_geometry(x_trans):
    width = 24*um

    th1 = 70*nm
    th2 = 2*um
    th3 = 1*um
    th4 = 500*nm

    tris = []

    # 70 nm multi patter layer
    mins = [-25*nm, -width, 0]
    maxs = [+25*nm, +width, th1]
    box_tri_mesh(mins, maxs, tris, 1)

    mins = [-125*nm, -width, 0]
    maxs = [-75*nm, +width, th1]
    box_tri_mesh(mins, maxs, tris, 6)

    mins = [75*nm, -width, 0]
    maxs = [125*nm, +width, th1]
    box_tri_mesh(mins, maxs, tris, 7)

    # 2 um Si3N4 layer
    mins = [-width, -width, th1]
    maxs = [+width, +width, th1 + th2]
    box_tri_mesh(mins, maxs, tris, 2)

    # 1 um SiO2 layer
    mins = [-width, -width, th1 + th2]
    maxs = [+width, +width, th1 + th2 + th3]
    box_tri_mesh(mins, maxs, tris, 3)

    # detector (ibody=4, mat=5)
    mins = [-width * 5, -width * 5, 3.20e-3]
    maxs = [+width * 5, +width * 5, 3.21e-3]
    box_tri_mesh(mins, maxs, tris, 4)

    # Nb wires
    nb_wires_(np.linspace(-16*um + x_trans, 8*um + x_trans, 4), 5, tris)

    # mapping ibody -> ds_max
    ds_maxs = {1: 7*nm, 2: 200*nm, 3: 50*nm, 5: 50*nm, 6: 7*nm, 7: 7*nm}

    # mapping ibody to mat
    body_to_mat = {1: 1, 2: 2, 3: 3, 4: 5, 5: 4, 6: 6, 7: 7}

    t = octree.Octree(7, body_to_mat, ds_maxs)
    t.set_bounds(-width * 5.01, -width * 5.01, -0.1*nm,
                 +width * 5.01, +width * 5.01, 32.2*um)
    t.build(tris)

    return t
