import time
import numpy as np
from scipy.sparse import lil_matrix

import matplotlib.pyplot as plt

import cvxpy as cp

from solid_angle import solid_angle


um = 1.0e-4
nm = 1.0e-7

voxel_width = 50*nm
dist_to_phantom = 2.07*um


def next_voxel(x, z, i, k, u, w):
    xmin = i * voxel_width
    xmax = (i + 1) * voxel_width
    zmax = dist_to_phantom + (k + 1) * voxel_width

    tz = (zmax - z) / w

    if u > 0:
        tx = (xmax - x) / u
        if tx < 0:
            raise ValueError
        if tx < tz:
            if i == 159:
                return tx, xmax - 8*um, z + w * tx, 0, k
            else:
                return tx, xmax, z + w * tx, i + 1, k
        else:
            return tz, x + tz * u, zmax, i, k + 1
    elif u < 0:
        tx = (xmin - x) / u
        if tx < tz:
            if i == 0:
                return tx, xmin + 8*um, z + w * tx, 159, k
            else:
                return tx, xmin, z + w * tx, i - 1, k
        else:
            return tz, x + tz * u, zmax, i, k + 1


# even in the angles
th_bins = np.linspace(-np.arctan(120 / 32), np.arctan(120 / 32), 161)
ths = (th_bins[1:] + th_bins[:-1]) / 2

# strip detector sizes are not uniform
x_bins = np.tan(th_bins) * 32  # Detectors are 32 um above.
xs = (x_bins[1:] + x_bins[:-1]) / 2


vertices_ny = np.vstack([
    x_bins * 1e-4,
    np.ones(161) * -5.0e-4,
    np.ones(161) * 32.0e-4
]).T

vertices_py = np.vstack([
    x_bins * 1e-4,
    np.ones(161) * +5.0e-4,
    np.ones(161) * 32.0e-4
]).T

sas = np.zeros(160)

for i in range(160):
    sa = 0
    sa += solid_angle(vertices_ny[i], vertices_ny[i + 1], vertices_py[i])
    sa += solid_angle(vertices_py[i], vertices_py[i + 1], vertices_ny[i + 1])
    sas[i] = sa

sinograms = np.zeros((160, 160))

for i, x_trans in enumerate(np.arange(-4000, 4000, 50)):
    dname = "x{0:_>+5d}nm".format(x_trans)
    meas = f[dname][:]
    g = (meas[:, 2] > -5*um) & (meas[:, 2] < 5*um)
    hist, _ = np.histogram(meas[g, 1] * 1e4, bins=x_bins)
    sinograms[i] = hist / sas


# l, m = 0, 0
def a_lm(l, m):
    """l is translations, m is angles
    a"""
    x = (159 - l + 0.5) * 5e-6
    th = ths[m]
    tan = np.tan(th)
    w = np.cos(th)
    u = np.sin(th)

    # first pixel
    x = (x + tan * dist_to_phantom) % 8e-4
    z = dist_to_phantom
    i = int(x // voxel_width)
    k = 0

    weight_indices = []
    while k < 20:
        t, x, z, n_i, n_k = next_voxel(x, z, i, k, u, w)
        weight_indices.append([t, i, k])
        i, k = n_i, n_k

    return weight_indices


log_sinograms = np.log(sinograms)
log_sinograms = log_sinograms - np.max(log_sinograms) - 1
# attn_densities = np.ones((160, 20)) * 1.0e5

n_rows = 160
row_offset = (160 - n_rows) // 2

a = lil_matrix((160 * n_rows, 160 * 20))

# l for translation, m for angle in b
# i for horizontal, j for vertical in x
# A((l, m),(i, j))
for l in range(0, 160):
    for m in range(n_rows):
        for t, i, j in a_lm(l, row_offset + m):
            a[l * n_rows + m, i * 20 + j] = -t


attn_densities = cp.Variable(3200)
mid_attn = cp.Variable()
range_attn = cp.Variable()

objective = cp.Minimize(cp.sum_squares(a @ attn_densities -
                                       log_sinograms[:, row_offset:row_offset + n_rows].reshape(160 * n_rows)) +
                        cp.total_variation.tv(cp.reshape(attn_densities, (160, 20))) * 5.0e-6)
prob = cp.Problem(objective)
prob.solve(verbose=True)


fig, ax = plt.subplots()
ax.tick_params(which='both', direction='in', right=True, top=True)

ax.imshow(attn_densities.value.reshape(160, 20).T, extent=(0.025, 8.025, 0, 1))

ax.set_title("iterative reconstruct of Nb wires")
ax.set_xlabel("x (um)")
ax.set_ylabel("z (um)")


fig, ax = plt.subplots()
ax.tick_params(which='both', direction='in', right=True, top=True)

th_range_in_deg = np.arctan(120 / 32) * 180 / np.pi
ax.imshow(sinograms[:, row_offset:row_offset + n_rows].T,
          extent=(-4, 4, th_bins[row_offset] * 180 / np.pi, th_bins[row_offset + n_rows] * 180 / np.pi), aspect="auto")

ax.set_xlabel("scan x offset (um)")
ax.set_ylabel("ray angle (deg)")
ax.set_title("conical beam sinogram (PENELOPE)")


fig, ax = plt.subplots()
ax.tick_params(which='both', direction='in', right=True, top=True)

th_range_in_deg = np.arctan(120 / 32) * 180 / np.pi
ax.imshow(np.exp(a.dot(attn_densities.value)).reshape((160, n_rows)).T,
          extent=(-4, 4, th_bins[row_offset] * 180 / np.pi, th_bins[row_offset + n_rows] * 180 / np.pi), aspect="auto")

ax.set_xlabel("scan x offset (um)")
ax.set_ylabel("ray angle (deg)")
ax.set_title("reconstructed conical beam sinogram")


sio2_attn = 405.68
nb_attn = 5813.95

v_material = cp.Variable(3200, boolean=True)
v_scale = cp.Variable()

objective = cp.Minimize(cp.sum_squares(a @ (v_material * (nb_attn - sio2_attn) + sio2_attn) -
                                       log_sinograms.reshape(160 * 160) - v_scale))
prob = cp.Problem(objective)
prob.solve(max_iters=1000, feastol=1, reltol=1, abstol=1, verbose=True)

fig, ax = plt.subplots()
ax.tick_params(which='both', direction='in', right=True, top=True)


ax.imshow(v_material.value.reshape(160, 20).T, extent=(0.025, 8.025, 0, 1))

ax.set_title("iterative reconstruct of Nb wires")
ax.set_xlabel("x (um)")
ax.set_ylabel("z (um)")
