import numpy as np
from numpy.linalg import norm


def solid_angle(va, vb, vc):
    """Solid angle subtended by va, vb, vc vectors
    from the perspective at  the origin.
    When two of them are nearly anti-parallel, the results
    might not be accurate. It could be 0 or 2pi.
    """
    va = va / norm(va)
    vb = vb / norm(vb)
    vc = vc / norm(vc)

    epsilon = 1.0e-12

    # If va, vb, vc are almost linearly dependent, treat them separately.
    if np.abs(np.dot(va, np.cross(vb, vc))) < epsilon:
        # (v1, v2) will be the pair having largest angle between them.
        v1, v2, v3 = va, vb, vc
        if np.dot(vb, vc) < np.dot(v1, v2):
            v1, v2, v3 = vb, vc, va
        if np.dot(vc, va) < np.dot(v1, v2):
            v1, v2, v3 = vc, va, vb

        # If the largest angle between two are smaller than 120 degree,
        # solid angle would be 0.
        if np.dot(v1, v2) > -1/2:
            return 0
        
        # If it's greater than 120 degree, the solid angle could be 2pi.
        if np.arccos(np.dot(v1, v3)) + np.arccos(np.dot(v2, v3)) > np.arccos(np.dot(v1, v2)):
            return np.pi * 2

    def angle(a, b, c):
        """Interior angle <bac of the geodesic triangle on the unit sphere.
        a, b, and c should be unit vectors.
        """
        # normal to a (so is tangent to the unit sphere.)
        # And lies in the plane defined by (a, b)
        s = b - np.dot(b, a)*a
        s = s / norm(s)

        # normal to a (so is tangent to the unit sphere.)
        # And lies in the plane defined by (a, c)
        t = c - np.dot(c, a)*a
        t = t / norm(t)

        return np.arccos(np.dot(s, t))

    # Sum of interior triangles - np.pi = solid angle subtended.
    return angle(va, vb, vc) + angle(vb, vc, va) + angle(vc, va, vb) - np.pi


def solid_angle_jump_at_vertex(dir, a, es):
    """Solid angle jump from just before a to right after a
    through direction dir.
    es can have an arbitrary number of edges of triangles
    containing the vertex a

    This function might have rounding off errors when dir and n are
    almost normal.
    """
    s_sum, d_sum = 0, 0

    for e in es:
        b, c = e
        n = np.cross(b - a, c - a)
        n = n / norm(n)
        s_sum -= solid_angle(dir, b - a, c - a) * np.sign(np.dot(dir, n))
        d_sum -= solid_angle(-dir, b - a, c - a) * np.sign(np.dot(-dir, n))

    return d_sum - s_sum


def solid_angle_jump_at_edge(dir, e, n1, n2):
    """e is the sharing edge. e assumed to be counterclockwise direction
    of the face of n1. For n2, -e is the counterclockwise direction.
    n1 and n2 are two normal vectors of two faces having e in common.
    """
    dir = dir / norm(dir)

    def angle(d):
        # l1 is normal to the plane containing edge e and the direction d
        l1 = np.cross(e, d)
        l1 = l1 / norm(l1)

        l2 = np.cross(-e, d)
        l2 = l2 / norm(l2)

        sa = 0
        sa += np.arccos(np.dot(n1, l1)) * np.sign(np.dot(d, n1)) * 2
        sa += np.arccos(np.dot(n2, l2)) * np.sign(np.dot(d, n2)) * 2

        return sa

    return angle(dir) - angle(-dir)


if __name__ == "__main__":
    o = np.asarray([0, 0, 0.01])
    dir = np.asarray([1, 0, 0])
    a = np.asarray([-1, -1, 0])
    b = np.asarray([1, 0, 0])
    c = np.asarray([0, 1, 0])

    # moller_trumbore(o, dir, a, b, c)

    j = solid_angle_jump_at_vertex(dir, o, [[b, c], [c, a], [a, b]])

    if j > np.pi * 2:
        print("Moving inward")
    elif j < -np.pi * 2:
        print("Moving outward")
    else:
        print("Staying in the same side")

    dir = np.asarray([0, 0, -1])
    e = np.asarray([0, -1, 0])
    n1 = np.asarray([0.2, 0, 1])
    n2 = np.asarray([-0.2, 0, 1])

    j = solid_angle_jump_at_edge(dir, e, n1, n2)
    if j > np.pi * 2:
        print("Moving inward")
    elif j < -np.pi * 2:
        print("Moving outward")
    else:
        print("Staying in the same side")

    #  The sum of solid angle of a geodesic triangle abc
    # and the solid angle of a related geodesic triangle b(-a)c
    # is 2 * the angle between the plane containing a and b
    # and the plane containing a and c.

    th1 = 0.5
    th2 = 1.2
    dphi = 1.0
    a = np.asarray([0, 0, 1])  # (th = 0, phi = 0)
    b = np.asarray([np.sin(th1), 0, np.cos(th1)])  # (th = th1, phi=0)
    c = np.asarray([np.sin(th2)*np.cos(dphi), np.sin(th2)*np.sin(dphi), np.cos(th2)])  # (th = th2, phi = dphi)

    print(solid_angle(a, b, c) + solid_angle(b, -a, c) - dphi * 2)

    v1 = np.cross(a, b)
    v1 = v1 / norm(v1)

    v2 = np.cross(a, c)
    v2 = v2 / norm(v2)

    print("angle <bac ", np.arccos(np.dot(v1, v2)), " == the solid angle / 2 ",
          (solid_angle(a, b, c) + solid_angle(b, -a, c))/2)

