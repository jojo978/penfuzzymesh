import heapq
import multiprocessing as mp

import h5py
import numpy as np
import zmq

import penelope

from penelope_geometry import build_nb_geometry
from penelope_main import MyMain


unit_of_works = 2048

port1 = 53556
port2 = 53557
port3 = 53558


def worker(geos):
    penelope.peinit(1.0e6, ["materials/Ti.mat",
                            "materials/Si3N4.mat",
                            "materials/SiO2.mat",
                            "materials/Nb.mat"], 1.0e3)

    penvared = penelope.Penvared()
    force = penvared.force

    # ICOL = 5, KPAR = 1, IBODY = 1
    force[4, 0, 0] = 100

    ctx = zmq.Context()
    sub = ctx.socket(zmq.SUB)
    sub.connect("tcp://localhost:{0:d}".format(port1))
    sub.setsockopt(zmq.SUBSCRIBE, b'')

    pull = ctx.socket(zmq.PULL)
    pull.connect("tcp://localhost:{0:d}".format(port2))

    push = ctx.socket(zmq.PUSH)
    push.connect("tcp://localhost:{0:d}".format(port3))

    poller = zmq.Poller()
    poller.register(sub, zmq.POLLIN)
    poller.register(pull, zmq.POLLIN)

    while True:
        socks = dict(poller.poll())

        if sub in socks:
            break
        elif pull in socks:
            h = pull.recv_json()
            geo, nsims, seed1, seed2 = h['geo'], h['nsims'], h['seed1'], h['seed2']

            penelope.set_seed(seed1, seed2)
            t = geos[geo]

            meas = []
            # penelope.sims(unit_of_works, t, meas)
            m = MyMain(t, meas)
            m.run(unit_of_works)

            # send back the results
            seed1, seed2 = penelope.get_seed()
            d = np.asarray(meas, dtype=np.float64)
            push.send_json({"geo": geo, "nsims": nsims + unit_of_works, "seed1": seed1, "seed2": seed2},
                           flags=zmq.SNDMORE)
            push.send_json({"nmeas": len(meas)}, flags=zmq.SNDMORE)
            push.send(d.data)

    sub.close()
    pull.close()
    push.close()
    ctx.destroy()
    ctx.term()


if __name__ == "__main__":
    scan_offsets = np.arange(-4000, 4000, 50)

    sim_pools = []

    try:
        with h5py.File("sims.h5", "r+") as f:
            for x_trans in scan_offsets:
                dset_name = "x{0:_>+5d}nm".format(x_trans)
                if dset_name in f:
                    dset = f[dset_name]
                    sim_pools.append([int(dset.attrs["nsims"][0]), dset_name,
                                      int(dset.attrs["seeds"][0]), int(dset.attrs["seeds"][1])])
    except OSError:
        with h5py.File("sims.h5", "w") as f:
            for x_trans in scan_offsets:
                dset_name = "x{0:_>+5d}nm".format(x_trans)
                dset = f.create_dataset(dset_name, dtype=np.float64,
                                        shape=(0, 5), chunks=(1024, 5), maxshape=(None, 5))

                dset.attrs.create('nsims', [0], shape=(1,), dtype=np.int32)
                dset.attrs.create("seeds", [1, 1], shape=(1, 2), dtype=np.int32)
                sim_pools.append([0, dset_name, 1, 1])

    heapq.heapify(sim_pools)

    ctx = zmq.Context()

    pub = ctx.socket(zmq.PUB)
    pub.bind("tcp://*:{0:d}".format(port1))

    push = ctx.socket(zmq.PUSH)
    push.bind("tcp://*:{0:d}".format(port2))

    pull = ctx.socket(zmq.PULL)
    pull.bind("tcp://*:{0:d}".format(port3))

    geos = dict()

    for x_trans in scan_offsets:
        geos["x{0:_>+5d}nm".format(x_trans)] = build_nb_geometry(x_trans)

    workers = []
    for _ in range(8):
        p = mp.Process(target=worker, args=(geos,))
        workers.append(p)
        p.start()

    # Push 8 tasks
    for _ in range(8):
        nsims, geo, seed1, seed2 = heapq.heappop(sim_pools)
        push.send_json({"geo": geo, "nsims": nsims, "seed1": seed1, "seed2": seed2})

    while True:
        h = pull.recv_json()
        mh = pull.recv_json()
        d = pull.recv()
        dat = np.frombuffer(d, dtype=np.float64).reshape((mh['nmeas'], 5))
        print(h['geo'], dat.shape, h['nsims'])

        # chunk of calculations are to be saved.
        with h5py.File("sims.h5", "r+") as f:
            dset = f[h['geo']]
            old_shape = dset.shape
            new_shape = (old_shape[0] + mh['nmeas'], old_shape[1])
            dset.resize(new_shape)
            dset[old_shape[0]:, :] = dat
            dset.attrs['nsims'] = np.asarray(h['nsims'], dtype=np.int32).reshape((1,))
            dset.attrs['seeds'] = np.asarray([h['seed1'], h['seed2']], dtype=np.int32).reshape((2,))

        if h['nsims'] < 1024 * 1024:
            heapq.heappush(sim_pools, [h['nsims'], h['geo'], h['seed1'], h['seed2']])

        # If some works still need to be done.
        if sim_pools:
            nsims, geo, seed1, seed2 = heapq.heappop(sim_pools)
            push.send_json({"geo": geo, "nsims": nsims, "seed1": seed1, "seed2": seed2})
        else:
            break

    # notify worker to stop
    pub.send(b'')

    # wait until all works stop
    for p in workers:
        p.join()

    pub.close()
    push.close()
    pull.close()

    ctx.destroy()
    ctx.term()
