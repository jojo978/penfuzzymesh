# distutils: language = c++

from libc.stdio cimport printf
from libc.math cimport sin, cos, sqrt
from libcpp.vector cimport vector
from libcpp.set cimport set

from penelope.core cimport *
from penelope.octree cimport Octree
from penelope.run cimport Main
from penelope.core cimport __track_mod_MOD_e as track_e
from penelope.core cimport __track_mod_MOD_x as track_x
from penelope.core cimport __track_mod_MOD_y as track_y
from penelope.core cimport __track_mod_MOD_z as track_z
from penelope.core cimport __track_mod_MOD_wght as track_wght
from penelope.core cimport __track_mod_MOD_ibody as track_ibody
from penelope.core cimport __track_mod_MOD_mat as track_mat
from penelope.core cimport __track_mod_MOD_ilb as track_ilb
from penelope.core cimport __track_mod_MOD_kpar as track_kpar
from penelope.core cimport __track_mod_MOD_ipol as track_ipol


DEF twopi = 6.283185307179586
DEF keV = 1.0e3
DEF um = 1.0e-4
DEF nm = 1.0e-3*um


class TrajectoryPoint:
    def __init__(self, prev, x, y, z):
        self.prev = prev
        self.x = x
        self.y = y
        self.z = z
        self.ds = 0
        self.de = 0.0
        self.icol = 0
        self.next = None
        self.trajectory = None
        self.body_inclusion = None

    def set_trajectory(self, trajectory):
        self.trajectory = trajectory

    def __repr__(self):
        return "TrajectoryPoint(x:{0: 9.3e}, y:{1: 9.3e}, z:{2: 9.3e}, de:{3: 9.3f}, icol:{4:d}, bodys:{5:s})"\
                   .format(self.x, self.y, self.z, self.de, self.icol, self.body_inclusion.__repr__())


class Trajectory:
    def __init__(self, parent, icol, kpar, p):
        self.parent = parent
        self.icol = icol
        self.kpar = kpar
        self.head = p
        self.current = p
        self.len = 1

    def add_point(self, p):
        self.current.next = p
        p.prev = self.current
        self.current = p
        self.len += 1

    def __getitem__(self, n):
        p = self.head

        if n < 0:
            n = self.len + n

        for i in range(n):
            if not p:
                break
            else:
                p = p.next

        return p

    def __repr__(self):
        str = ""
        p = self.head
        while p:
            str += p.__repr__() + "\n"
            p = p.next

        return str


cdef class MyMain(Main):
    cdef list meas
    # cdef public list init_point_stack
    # cdef public list trajs

    def __init__(self, t, meas):
        self.meas = meas
        # self.init_point_stack = [None] * NMS
        # self.trajs = []

    cdef void init_primary_particle(self):
        cdef double e, x, y, z, u, v, w, wght
        cdef int ilb[5]
        cdef int kpar, pol
        cdef int ibody, mat

        global track_ibody, track_mat

        e = 15*keV
        x, y, z = 0, 0, 1.0e-4*nm
        u, v, w = 0, 0, 1.0
        wght = 1.0
        kpar = 1  # electron
        pol = 0

        ilb = [1, 0, 0, 0, 0]

        # stores_ pushes this PENELOPE module variables into the secondary particle stack.
        track_ibody = 1
        track_mat = 1

        stores_(&e, &x, &y, &z, &u, &v, &w, &wght, &kpar, &ilb[0], &pol)
        self.flight_logs[0] = {1}

    cdef void start_of_track(self):
        global track_wght
        cdef double ar, ath, au, av, aw
        cdef int nsplits = 100

        if track_ilb[3] // 1000000 == 22 and track_kpar == 2:
            if track_ilb[2] != 9:
                track_ilb[2] = 9
                track_wght = track_wght / nsplits

                for _ in range(nsplits - 1):
                    aw = rand_() * 2.0 - 1.0
                    ar = sqrt(1 - aw*aw)
                    ath = rand_() * twopi
                    au = ar * cos(ath)
                    av = ar * sin(ath)
                    stores_(&track_e, &track_x, &track_y, &track_z,
                            &au, &av, &aw, &track_wght,
                            &track_kpar, track_ilb, &track_ipol)
                    self.flight_logs[secst_.nsec - 1] = self.t.body_inclusion_

    cdef bint cross_boundary(self):
        """This method simulates a non-physical detector.
        
        It simulates a detector (ibody=4) that absorbs every particle that
        impinges on it instantly at its surface.
        But it records only photons (kpar=2).
        """
        if track_ibody == 4:
            if track_kpar == 2:
                self.meas.append([track_e, track_x, track_y, track_z, track_wght])
            return True

        return False
