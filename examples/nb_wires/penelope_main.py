import penelope as pe
from penelope.core import Par

import matplotlib.pyplot as plt
import numpy as np

from nb_wire_geometry import build_geometry
from sim_main import MyMain


um = 1e-4
nm = 1e-3*um
eV = 1.0
keV = 1.0e3*eV
MeV = 1.0e3*keV

# all 4 materials share same eabs, wcc, wcr
pe.penelope.eabs.loc[1:4] = 1*keV
pe.penelope.wcc.loc[1:4] = 10*eV
pe.penelope.wcr.loc[1:4] = 10*eV
pe.peinit(1*MeV, ["../materials/Ti.mat",
                  "../materials/Si3N4.mat",
                  "../materials/SiO2.mat",
                  "../materials/Nb.mat"], 1)

pe.set_seed(1, 1)

# python binding to PENELOPE module variables.
track = pe.track
penvared = pe.penvared

# Interaction forcing setting.
force = penvared.force

# ICOL = 5 (core ionization interaction), KPAR = 1 (election),
# IBODY = 1 (Ti layer)
# 100 x more likely Ti atom ionization due to electrons
force.loc[5, (Par.electron, 1)] = 100

# build an octree of Nb wire structures with 0 um offset
t = build_geometry(-2000)

meas = []

# MyMain also split single Ti characteristic emission line into 100 of them
# Total 100 (interaction forcing) * 100 (splitting) x more likely Ti emissions
m = MyMain(t, meas)

for i in range(1, 21):
    m.run(1000)
    print("{: 3d}% ".format(i * 5) + "#" * i, end='\r', flush=True)


# MyMain records impinging photon locations and energies at the detector
fig, ax = plt.subplots()
ax.set_aspect('equal')
d = np.asarray(meas)
# ax.scatter([r[1]/um for r in meas], [r[2]/um for r in meas], s=1, alpha=0.2)
ax.scatter(d[:, 1]/um, d[:, 2]/um, s=1, alpha=0.2)

ax.tick_params(which='both', direction='in', right=True, top=True)
ax.set_xlim(-120, 120)
ax.set_ylim(-120, 120)
