# distutils: language = c++
from libc.math cimport sqrt, sin, cos
from libcpp.set cimport set

from penelope.core cimport *
from penelope.run cimport Main
from penelope.core cimport __track_mod_MOD_e as track_e
from penelope.core cimport __track_mod_MOD_x as track_x
from penelope.core cimport __track_mod_MOD_y as track_y
from penelope.core cimport __track_mod_MOD_z as track_z
from penelope.core cimport __track_mod_MOD_wght as track_wght
from penelope.core cimport __track_mod_MOD_ibody as track_ibody
from penelope.core cimport __track_mod_MOD_mat as track_mat
from penelope.core cimport __track_mod_MOD_ilb as track_ilb
from penelope.core cimport __track_mod_MOD_kpar as track_kpar
from penelope.core cimport __track_mod_MOD_ipol as track_ipol


DEF twopi = 6.283185307179586
DEF eV = 1.0
DEF keV = 1.e3*eV
DEF cm = 1.0
DEF um = 1.0e-4*cm
DEF nm = 1.0e-3*um


cdef class MyMain(Main):
    cdef list meas

    def __init__(self, t, meas):
        self.meas = meas

    cdef void init_primary_particle(self):
        cdef double e, x, y, z, u, v, w, wght
        cdef int ilb[5]
        cdef int kpar, pol
        cdef set[int] containing_bodies

        global track_ibody, track_mat

        e = 40*keV
        x, y, z = 0, 0, 21*nm - 1e-4*nm
        u, v, w = 0, 0, -1.0
        wght = 1.0
        kpar = 1
        pol = 0

        ilb = [1, 0, 0, 0, 0]

        # penelope.core.stores_ pushes following PENELOPE module variables
        # into the secondary particle stack.
        track_ibody, track_mat = 1, 1

        stores_(&e, &x, &y, &z, &u, &v, &w, &wght, &kpar, &ilb[0], &pol)
        containing_bodies.insert(1)
        self.flight_logs[0] = containing_bodies

    cdef void start_of_track(self):
        global track_wght

        cdef int nsplits = 100

        if (track_ilb[3] // 1000000 == 15 or track_ilb[3] // 1000000 == 5) and track_kpar == 2:
            # phosphorous or boron emission splits into nsplits photons.
            # track_wght is also reduce by nsplits.
            if track_ilb[2] != 9:
                # this photon won't be split again next time.
                track_ilb[2] = 9
                track_wght = track_wght / nsplits

                # (nsplits - 1) photons are pushed into
                # the secondary particle stack with random directions.
                for _ in range(nsplits - 1):
                    aw = rand_() * 2.0 - 1.0
                    ar = sqrt(1 - aw*aw)
                    ath = rand_() * twopi
                    au = ar * cos(ath)
                    av = ar * sin(ath)
                    stores_(&track_e, &track_x, &track_y, &track_z,
                            &au, &av, &aw, &track_wght,
                            &track_kpar, track_ilb, &track_ipol)
                    self.flight_logs[secst_.nsec - 1] = self.t.body_inclusion

    cdef bint cross_boundary(self):
        if track_ibody == 4:
            # when the particle touches the body 4, it's assumed to be absorbed
            # instantly and completely.
            if track_kpar == 2:
                self.meas.append([track_e, track_x, track_y, track_z, track_wght])
            return True

        return False
