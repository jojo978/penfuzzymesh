import numpy as np

import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

import h5py

import pandas as pd


with h5py.File('B0.03Si0.97_80keV.h5', 'r') as f:
    b_doped_data = f['photons'][:]
    b_num_events = f['photons'].attrs['nevents']

with h5py.File('P0.03Si0.97.h5', 'r') as f:
    p_doped_data = f['photons'][:]
    p_num_events = f['photons'].attrs['nevents']

fig, ax = plt.subplots(figsize=(12, 4))


def histogram(data, xmin=0, xmax=3000, num=301):
    bins = np.linspace(xmin, xmax, num)
    cuts = pd.cut(data[:, 0], bins=bins)
    y = pd.DataFrame(data[:, -1]).groupby(cuts).sum().to_numpy().flatten()
    x = (bins[1:] + bins[:-1]) / 2

    return x, y


for data, nevents, label in [[b_doped_data,
                              b_num_events,
                              r"B$_{0.03}$Si$_{0.97}$"],
                             [p_doped_data,
                              p_num_events,
                              r"P$_{0.03}$Si$_{0.97}$"]]:
    x, y = histogram(data)
    ax.step(x, y / nevents * 1e8, where='mid', label=label, linewidth=0.75)

ax.tick_params(which='both', direction='in', right=True, top=True)
ax.set_yscale("log")

ax.xaxis.set_minor_locator(MultipleLocator(100))

ax.set_title("Spectra per 100 M 80 keV electrons")
ax.set_xlabel("Energy (eV)")
ax.set_ylabel("Accumulated weight (per 10 eV bin)")
ax.set_xlim(100, 3000)

ax.legend(frameon=False, numpoints=1)


# Boron doped 40 keV vs 80 keV

with h5py.File('B0.03Si0.97_80keV.h5', 'r') as f:
    b_80_data = f['photons'][:]
    b_80_nevents = f['photons'].attrs['nevents']

with h5py.File('B0.03Si0.97_40keV.h5', 'r') as f:
    b_40_data = f['photons'][:]
    b_40_nevents = f['photons'].attrs['nevents']

fig, ax = plt.subplots(figsize=(12, 4))

for data, nevents, label in [[b_80_data, b_80_nevents, '80 keV'],
                             [b_40_data, b_40_nevents, '40 keV']]:
    x, y = histogram(data)
    ax.step(x, y / nevents * 1e8, where='mid', label=label, linewidth=0.75)


ax.tick_params(which='both', direction='in', right=True, top=True)
ax.set_yscale("log")

ax.xaxis.set_minor_locator(MultipleLocator(100))

ax.set_title("Spectra per 100 M electrons")
ax.set_xlabel("Energy (eV)")
ax.set_ylabel("Accumulated weight (per 10 eV bin)")
ax.set_xlim(100, 3000)

ax.legend(frameon=False, numpoints=1)


# Write csv files
x, p_hist = histogram(p_doped_data, 0, 12000, 1201)
out_fname = 'p_0.03si_0.97_{0:d}electrons_80keV.csv'.format(p_num_events)
with open(out_fname, 'w') as f:
    print('energy (eV),sum of wghts', file=f)
    for e, wght in zip(x, p_hist):
        print("{0: 15.4f},{1: 15.4f}".format(e, wght), file=f)

x, b_hist = histogram(b_doped_data, 0, 12000, 1201)
out_fname = 'b_0.03si_0.97_{0:d}electrons_80keV.csv'.format(b_num_events)
with open(out_fname, 'w') as f:
    print('energy (eV),sum of wghts', file=f)
    for e, wght in zip(x, b_hist):
        print("{0: 15.4f},{1: 15.4f}".format(e, wght), file=f)
