import time

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import pandas as pd

import h5py

import penelope as pe
from penelope.core import Par
from penelope.mesh import box_tri_mesh
from penelope import octree

from doped_silicon_main import MyMain

eV = 1.0
keV = 1.0e3*eV
MeV = 1.0e3*keV
um = 1.0e-4
nm = 1.0e-7


# 3% boron doped or 3% phosphorus doped silicon
def init(sample_name):
    # MAT=1 eabs, wcc, wcr setting
    # these simulation parameters need to be loaded before calling peinit.
    mat = 1
    pe.penelope.eabs.loc[mat] = 100*eV

    # How small wcc, wcr should be?
    pe.penelope.wcc.loc[mat] = 10*eV
    pe.penelope.wcr.loc[mat] = 10*eV

    pe.peinit(1*MeV, ["../materials/{0:s}.mat".format(sample_name)], 1)
    pe.set_seed(1, 1)

    # Interaction forcing setting.
    force = pe.penvared.force

    # ICOL = 5 (core ionization interaction),
    # KPAR = 1 (election), IBODY = 1 (Si)
    # 100 x more likely Ti atom ionizations due to electrons
    ibody = 1
    force.loc[5, (Par.electron, ibody)] = 100

    # ICOL = 4 (hard bremsstrahlung emissions),
    # KPAR = 1 (electron), IBODY = 1 (Si)
    force.loc[4, (Par.electron, ibody)] = 100


def do_sim(duration, sample_name):
    def create_or_open(sample_name):
        try:
            f = h5py.File("{0:s}.h5".format(sample_name), "r+")
        except OSError:
            f = h5py.File("{0:s}.h5".format(sample_name), "w")
            f.create_dataset("photons", shape=(0, 5), dtype=np.float32,
                             chunks=(1024, 5), maxshape=(None, 5))
            f['photons'].attrs['nevents'] = 0
        return f

    def append_data_to_h5(data, seeds, nevents):
        with create_or_open(sample_name) as f:
            data = np.asarray(data, dtype=np.float32)
            dset = f["photons"]
            old_shape = dset.shape
            new_shape = (old_shape[0] + data.shape[0], data.shape[1])
            dset.resize(new_shape)
            dset[old_shape[0]:] = data
            dset.attrs["seeds"] = seeds
            dset.attrs["nevents"] += nevents

    # build a triangular mesh
    tris = []
    # Si target
    box_tri_mesh([-1*um, -1*um, 0], [1*um, 1*um, 21*nm], tris, 1)
    # detector body
    box_tri_mesh([-1*um, -1*um, 22*nm], [1*um, 1*um, 23*nm], tris, 4)

    # mapping ibody -> mat
    body_to_mat = {1: 1, 4: 5}

    # mapping ibody -> ds_max
    ds_maxs = {1: 2.1*nm}

    # octree maxlevel = 7
    t = octree.Octree(7, body_to_mat, ds_maxs)
    t.set_bounds(-1.1*um, -1.1*um, -1*nm, 1.1*um, 1.1*um, 24*nm)
    t.build(tris)

    meas = []
    m = MyMain(t, meas)
    start_time = time.time()

    while time.time() - start_time < duration:
        num_events = 200000
        meas.clear()
        m.run(num_events)
        print(time.time() - start_time, num_events, len(meas))
        if meas:
            append_data_to_h5(meas, pe.get_seed(), num_events)


# MyMain records impinging photon locations and energies at the detector
def show_spec(sample_name):
    fig, ax = plt.subplots(figsize=(12, 4))
    ax.xaxis.set_minor_locator(MultipleLocator(100))

    with h5py.File("{0:s}.h5".format(sample_name), "r") as f:
        data = f['photons'][...]
        num_events = f['photons'].attrs['nevents']

    bins = np.linspace(0, 3000, 301)
    cuts = pd.cut(data[:, 0], bins=bins)
    y = pd.DataFrame(data[:, -1]).groupby(cuts).sum().to_numpy().flatten()
    x = (bins[1:] + bins[:-1]) / 2

    ax.step(x, y, where='mid')

    ax.tick_params(which='both', direction='in', right=True, top=True)

    ax.set_yscale("log")

    ax.set_xlim(100, 3000)
    ax.set_ylim(np.min(y) / 3, np.max(y) * 3)

    ax.set_title("{0:s} {1:d}M incident electrons of 80 keV".
                 format(sample_name, int(num_events // 1000000)))
    ax.set_xlabel("Energy (eV)")
    ax.set_ylabel("Accumulated weights (count per 10 eV bin)")

    return fig, ax
