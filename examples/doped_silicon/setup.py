from setuptools import setup
from setuptools.extension import Extension
from Cython.Build import cythonize

import numpy as np


exts = [Extension("doped_silicon_main",
                  ["doped_silicon_main.pyx"],
                  include_dirs=["../../external/penelope/include",
                                np.get_include()],
                  libraries=["penelope"])]

setup(ext_modules=cythonize(exts,
                            compiler_directives={'language_level': 3}))
