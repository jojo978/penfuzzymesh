# distutils: language = c++
from libc.math cimport sqrt, sin, cos
from libcpp.set cimport set

from penelope.core cimport *
from penelope.run cimport Main
from penelope.core cimport (__track_mod_MOD_e as track_e,
                            __track_mod_MOD_x as track_x,
                            __track_mod_MOD_y as track_y,
                            __track_mod_MOD_z as track_z,
                            __track_mod_MOD_wght as track_wght,
                            __track_mod_MOD_ibody as track_ibody,
                            __track_mod_MOD_mat as track_mat,
                            __track_mod_MOD_ilb as track_ilb,
                            __track_mod_MOD_kpar as track_kpar,
                            __track_mod_MOD_ipol as track_ipol)

from penelope.octree cimport Octree


DEF twopi = 6.283185307179586
DEF eV = 1.0
DEF keV = 1.e3*eV
DEF cm = 1.0
DEF um = 1.0e-4*cm
DEF nm = 1.0e-3*um


cdef class MyMain(Main):
    cdef list meas
    cdef Octree ot

    def __init__(self, t, meas):
        self.meas = meas
        self.ot = t

    cdef void init_primary_particle(self):
        cdef double e, x, y, z, u, v, w, wght
        cdef int ilb[5]
        cdef int kpar, pol
        cdef set[int] containing_bodies

        global track_ibody, track_mat

        e = (100 + rand_() * 10)*eV
        x, y, z = 0, 0, 1e-4*nm
        u, v, w = 0, 0, 1.0
        wght = 1.0
        kpar = 2  # photons
        pol = 0

        ilb = [1, 0, 0, 0, 0]

        # penelope.core.stores_ pushes following PENELOPE module variables
        # into the secondary particle stack.
        # You can use self.oc.locate to get the right ibody and mat for (x, y, z).
        # track_ibody, track_mat = self.ot.locate(x, y, z))
        track_ibody, track_mat = 1, 1

        stores_(&e, &x, &y, &z, &u, &v, &w, &wght, &kpar, &ilb[0], &pol)

        # if you used self.ot.locate earlier,
        # you can get the containing_bodies using self.oc.body_inclusions_.
        # self.flight_logs[0] = self.oc.body_inclusions_
        containing_bodies.insert(1)
        self.flight_logs[0] = containing_bodies

    cdef bint cross_boundary(self):
        if track_ibody == 4:
            # when the particle touches the body 4, it's assumed to be absorbed
            # instantly and completely. It's non-physical though
            if track_kpar == 2:
                self.meas.append([track_e, track_x, track_y, track_z, track_wght])
            return True

        return False
