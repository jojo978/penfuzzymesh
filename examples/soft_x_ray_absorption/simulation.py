import penelope as pe
from penelope.core import Par
from penelope.mesh import box_tri_mesh
from penelope import octree

from main import MyMain

eV = 1.0
keV = 1.0e3*eV
MeV = 1.0e3*keV
um = 1.0e-4
nm = 1.0e-7

# MAT=1 eabs, wcc, wcr setting
# these simulation parameters need to be loaded before calling peinit.
pe.penelope.eabs.loc[1] = 100*eV

# How small wcc, wcr should be?
pe.penelope.wcc.loc[1] = 10*eV
pe.penelope.wcr.loc[1] = 10*eV

pe.peinit(1*MeV, ["../materials/Si.mat"], 1)
pe.set_seed(1, 1)

# Interaction forcing setting.
force = pe.penvared.force

# ICOL = 5 (core ionization interaction), KPAR = 1 (election), IBODY = 1 (Si)
# 100 x more likely Ti atom ionizations due to electron bombardments
ibody = 1
force[5, (Par.electron, ibody)] = 100

# ICOL = 4 (hard bremsstrahlung emissions), KPAR = 1 (electron), IBODY = 1 (Si)
force[4, (Par.electron, ibody)] = 100

# build a triangular mesh
tris = []
# 21 nm thick Si
box_tri_mesh([-1*um, -1*um, 0], [1*um, 1*um, 21*nm], tris, 1)

# detector body, 4 is just an arbitrary choice.
box_tri_mesh([-1*um, -1*um, 22*nm], [1*um, 1*um, 23*nm], tris, 4)

# mapping ibody -> mat
# 4 -> 5 is an arbitrary choice, mat 5 should not be used by PENELOPE
# because MyMain stops tracking as soon as a particle touches the ibody 4.
body_to_mat = {1: 1, 4: 5}

# mapping ibody -> ds_max
ds_maxs = {1: 2.1*nm}

# octree maxlevel = 7
t = octree.Octree(7, body_to_mat, ds_maxs)
t.set_bounds(-1.1*um, -1.1*um, -1*nm, 1.1*um, 1.1*um, 24*nm)
t.build(tris)

meas = []
m = MyMain(t, meas)

num_events = 200000
m.run(num_events)
