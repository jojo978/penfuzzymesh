from setuptools import setup
from setuptools.extension import Extension
from Cython.Build import cythonize

import numpy as np


exts = [Extension("main",
                  ["main.pyx"],
                  include_dirs=["../../external/penelope/include",
                                np.get_include()],
                  libraries=["penelope"])]

setup(ext_modules=cythonize(exts,
                            compiler_directives={'language_level': 3}))
