from libcpp.set cimport set
from libcpp.vector cimport vector
from libcpp.map cimport map

cimport numpy as cnp
from .triangle cimport Triangle

cdef:
    struct Node:
        double xmin
        double ymin
        double zmin
        double xmax
        double ymax
        double zmax
        int level
        int num_tris
        # Node owns triangles and children nodes
        Triangle* tris  # triangles that overlap with this node
        Node* children_[8]
        # Node does not own neighbor nodes
        Node* neighbors_[6]


cdef class NodeWrap:
    cdef Node* pnode

    @staticmethod
    cdef NodeWrap from_ptr(Node*)


cdef Node* find_node(Node* node, double x, double y, double z, int level=*)


cdef class Octree:
    cdef Node* root
    cdef Node* node
    cdef int maxlevel
    cdef int num_nodes_
    cdef set[int] body_inclusion_
    cdef map[int,int] mats  # body to mat
    cdef map[int,double] ds_maxs  # body to ds_max
    cdef map[int,vector[Triangle]] body_meshes  # body to triangles to mesh of body
    cdef:
        double* px
        double* py
        double* pz
        double* pu
        double* pv
        double* pw
        int* pibody
        int* pmat

    cpdef int get_current_body(self)
    cdef void locate_(self, double x, double y, double z, int* body, int* mat)
    cdef double ds_wall_(self, double x, double y, double z, double u, double v, double w, int* num_neighbor)
    cdef double ds_triangle_(self, double* o, double* d, Triangle** intersected_tri)
    cdef int step_(self, double* x, double* y, double* z, double u, double v, double w, double ds, double* dsef, int* body, int* mat)
