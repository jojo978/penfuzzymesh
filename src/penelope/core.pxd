cdef:
    enum:
        MAXMAT = 10

    enum:
        NMS = 10000

    enum:
        NBV = 5000

cdef extern from "penelope.h":
    double __track_mod_MOD_x
    double __track_mod_MOD_y
    double __track_mod_MOD_z
    double __track_mod_MOD_e
    double __track_mod_MOD_u
    double __track_mod_MOD_v
    double __track_mod_MOD_w
    double __track_mod_MOD_wght
    int __track_mod_MOD_kpar
    int __track_mod_MOD_ibody
    int __track_mod_MOD_mat
    int __track_mod_MOD_ilb[5]
    int __track_mod_MOD_ipol

    int __track_mod_MOD_mhinge
    double __track_mod_MOD_e0segm
    double __track_mod_MOD_desoft
    double __track_mod_MOD_ssoft

    double __penelope_mod_MOD_eabs[MAXMAT][3]
    double __penelope_mod_MOD_c1[MAXMAT]
    double __penelope_mod_MOD_c2[MAXMAT]
    double __penelope_mod_MOD_wcc[MAXMAT]
    double __penelope_mod_MOD_wcr[MAXMAT]

    double __penvared_mod_MOD_force[8][3][NBV]
    int __penvared_mod_MOD_ibrspl[NBV];

    struct seed:
        int iseed1
        int iseed2

    seed rseed_

    struct secst:
        double es[NMS]
        double xs[NMS]
        double ys[NMS]
        double zs[NMS]
        double us[NMS]
        double vs[NMS]
        double ws[NMS]
        double wghts[NMS]
        double sp1s[NMS]
        double sp2s[NMS]
        double sp3s[NMS]
        double pages[NMS]
        int kps[NMS]
        int ibodys[NMS]
        int ms[NMS]
        int ilbs[NMS][5]
        int ipols[NMS]
        int nsec

    secst secst_

    void peinit_(double*,int*,int*,int*,char*);
    void cleans_()
    void start_()
    void secpar_(int*)
    void jump_(double*,double*)
    void knock_(double*,int*)
    void stores_(double*,double*,double*,double*,double*,double*,double*,double*,int*,int*,int*)
    double rand_()

    void jumpf_(double*,double*)
    void knockf_(double*,int*)
    void vsplit_(int*)
    void vrr_(double*)
    void vkill_(double*)

    void panar_(double* ecut)


cdef class DoubleAttr:
    cdef double *p

    @staticmethod
    cdef DoubleAttr create(double *p)


cdef class IntAttr:
    cdef int *p

    @staticmethod
    cdef IntAttr create(int *p)
