from libcpp.set cimport set

cimport numpy as cnp

cdef:
    ctypedef double vec3[3]

    struct Triangle:
        double a[3]
        double b[3]
        double c[3]
        double normal[3]
        int body
        bint is_outside  # flag indicates if this triangle is for outside points or inside.


cdef class TriangleWrap:
    cdef Triangle tri

    @staticmethod
    cdef cnp.ndarray[cnp.float64_t, ndim=1] pd_to_ndarray(double[:])

    @staticmethod
    cdef TriangleWrap from_ptr(Triangle *)


cdef void subtract(double *ret, double *a, double *b)
cdef void add(double *ret, double *a, double *b)
cdef void multiply(double *ret, double c, double *a)
cdef void cross_product(double *ret, double *a, double *b)
cdef cross_prod_z(double *a, double *b)
cdef double dot_product(double *a, double *b)
cdef void normalize(double *a)

cdef double distance_to_triangle_(double* x, Triangle *tri)
cdef void init_triangle(Triangle* t, triangle)
cdef Triangle* new_triangle(triangle)
cdef double wbw_intersect_(double *o, double *d, Triangle*tris, int num_tris,
                           set[int]& body_inclusion, Triangle **intersected_tri)
