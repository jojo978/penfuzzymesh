# distutils: language = c++

from libc.math cimport sqrt, fabs
from libc.stdlib cimport malloc, free

cimport numpy as cnp
import numpy as np


cdef inline void subtract(double* ret, double* a, double* b):
    ret[0] = a[0] - b[0]
    ret[1] = a[1] - b[1]
    ret[2] = a[2] - b[2]


cdef inline void add(double* ret, double* a, double* b):
    ret[0] = a[0] + b[0]
    ret[1] = a[1] + b[1]
    ret[2] = a[2] + b[2]


cdef inline void multiply(double* ret, double c, double* a):
    ret[0] = c * a[0]
    ret[1] = c * a[1]
    ret[2] = c * a[2]


cdef inline void cross_product(double* ret, double* a, double* b):
    ret[0] = a[1] * b[2] - a[2] * b[1]
    ret[1] = a[2] * b[0] - a[0] * b[2]
    ret[2] = a[0] * b[1] - a[1] * b[0]


cdef inline cross_prod_z(double* a, double* b):
    return a[0] * b[1] - a[1] * b[0]


cdef inline double dot_product(double* a, double* b):
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]


cdef inline void normalize(double* a):
    cdef l
    l = sqrt(dot_product(a, a))
    a[0] = a[0] / l
    a[1] = a[1] / l
    a[2] = a[2] / l


cdef inline double distance_p_to_edge(double* x, double t, double* a, double* b):
    """Distance from x to (1 - t) * a + t * b
    """
    cdef double[3] v
    cdef double[3] w

    multiply(v, 1 - t, a)  # v = (1 - t) * a
    multiply(w, t, b)  # w = t * b
    add(v, v, w)  # v = v + w
    subtract(v, x, v)  # v = x -v
    return sqrt(dot_product(v, v))  # sqrt(v * v)


def distance_to_triangle(double[:] o, triangle):
    """Python interface to distance_to_triangle_

    Args:
        o (numpy.ndarray(shape=(3,), dtype=float64)):
        triangle (object): compatible with init_triangle

    Returns:
        float: distance to a given triangle.
    """
    cdef Triangle tri
    init_triangle(&tri, triangle)
    return distance_to_triangle_(&o[0], &tri)


cdef double distance_to_triangle_(double* x, Triangle* tri):
    """Calculate the distance from a point to a triangle
    
    7 regions are considered in this implementation.
    
    Args:
        x (double[3]): coordinates of a given point
        tri (Triangle*):
    """
    cdef double ab[3]
    cdef double bc[3]
    cdef double ca[3]

    cdef double n[3]
    cdef double v[3]
    cdef double w[3]

    subtract(ab, tri.b, tri.a)  # ab = tri.b - tri.a
    subtract(bc, tri.c, tri.b)
    subtract(ca, tri.a, tri.c)

    cdef double dot_a_ab, pa
    cdef double dot_b_bc, pb
    cdef double dot_c_ca, pc

    # projection of x to the ab edge.
    dot_a_ab = dot_product(tri.a, ab)
    pa = (dot_product(x, ab) - dot_a_ab) / (dot_product(tri.b, ab) - dot_a_ab)

    # projection of x to the bc edge
    dot_b_bc = dot_product(tri.b, bc)
    pb = (dot_product(x, bc) - dot_b_bc) / (dot_product(tri.c, bc) - dot_b_bc)

    # projection of x to the ca edge.
    dot_c_ca = dot_product(tri.c, ca)
    pc = (dot_product(x, ca) - dot_c_ca) / (dot_product(tri.a, ca) - dot_c_ca)

    if pa >= 1 and pb <= 0:
        # distance between x and tri.b
        subtract(v, x, tri.b)
        return sqrt(dot_product(v, v))
    elif pb >=1 and pc <= 0:
        # distance between x and tri.c
        subtract(v, x, tri.c)
        return sqrt(dot_product(v, v))
    elif pc >= 1 and pa <= 0:
        # distance between x and tri.a
        subtract(v, x, tri.a)
        return sqrt(dot_product(v, v))

    cross_product(n, ab, tri.normal)
    if dot_product(x, n) > dot_product(tri.a, n):
        # distance x to the point on the edge ba
        return distance_p_to_edge(x, pa, tri.a, tri.b)

    cross_product(n, bc, tri.normal)
    if dot_product(x, n) > dot_product(tri.b, n):
        # distance x to the point on the edge bc
        return distance_p_to_edge(x, pb, tri.b, tri.c)

    cross_product(n, ca, tri.normal)
    if dot_product(x, n) > dot_product(tri.c, n):
        # distance x to the point on the edge ca
        return distance_p_to_edge(x, pc, tri.c, tri.a)

    #distanace x to the interior point of tri
    subtract(v, x, tri.a)
    return fabs(dot_product(v, tri.normal))


cdef void init_triangle(Triangle* t, triangle):
    """Initialize an allocated memory for a struct Triangle
    
    Args:
        t(Triangle*) : pre-allocated memory for a Triangle
        triangle : andy python object represents a triangle.
    """
    cdef:
        double eba[3]
        double eca[3]
        cdef TriangleWrap tri_wrap

    if isinstance(triangle, TriangleWrap):
        tri_wrap = triangle
        t[0] = tri_wrap.tri
    else:
        t.a = triangle[0]
        t.b = triangle[1]
        t.c = triangle[2]

        subtract(eba, t.b, t.a)
        subtract(eca, t.c, t.a)
        cross_product(t.normal, eba, eca)
        normalize(t.normal)

        t.body = triangle[3]
        t.is_outside = triangle[4]


cdef Triangle* new_triangle(triangle):
    """Constructor for Triangle
    
    Args:
        triangle (object): A Python object represents a Triangle. This could be
                [va(double[3]), vb(double[3]), vc(double[3]), body(int), is_outside(bool)] or
                TriangleWrap instance
        
    Returns:
        Triangle*: created and initialized Triangle instance 
    """
    cdef Triangle* t

    t = <Triangle*>malloc(sizeof(Triangle))
    init_triangle(t, triangle)

    return t


cdef inline void orient_along_max_dim(double* d, int* pkx, int* pky, int* pkz):
    """Find the right-handed orientation along the z axis of which has the largest absolute component.
    
    This means that abs(d[pkz[0]]) is bigger or equal to abs(d[pkx[0]]) and abs(d[pky[0]]).
    """
    cdef int kx, ky, kz

    # find the axis has the largest component
    if abs(d[2]) > abs(d[1]) and abs(d[2]) > abs(d[0]):
        kz = 2
    elif abs(d[1]) > abs(d[0]):
        kz = 1
    else:
        kz = 0

    kx = (kz + 1) % 3
    ky = (kz + 2) % 3

    # keep the right-handed coordinate system
    if d[kz] < 0:
        kx, ky = ky, kx

    pkx[0] = kx
    pky[0] = ky
    pkz[0] = kz


def wbw_intersect(double[:] o, double[:] d, triangles, bf):
    """Python interface to wbw_intersect_

    Args:
        o (double[:] or compatible): the starting point
        d (double[:] or compatible): the direction of the ray
        triangles (list of objects): a list of triangles, which is application to init_triangle
        bf (set like object, which is compatible with std:set[int]): set of bodies containing the starting point

    Returns:
        float: the distance to the nearest interacting triangle
        object : the nearest intersecting triangle out of the given triangles.
    """
    cdef set[int] body_inclusion
    cdef Triangle* tris
    cdef Triangle* tri
    cdef int i

    tris = NULL
    try:
        tris = <Triangle*>malloc(sizeof(Triangle) * len(triangles))
        if not tris:
            raise MemoryError()

        for i in range(len(triangles)):
            init_triangle(&tris[i], triangles[i])

        ds_triangle = wbw_intersect_(&o[0], &d[0], tris, len(triangles), bf, &tri)

        if tri != NULL:
            return ds_triangle, triangles[tri - tris]
    finally:
        free(tris)


cdef inline double wbw_intersect_(double* o, double* d, Triangle* tris, int num_tris, set[int]& body_inclusion, Triangle** intersected_tri):
    """Watertight ray tracing implementation
    
    Args:
        o (double[3]): the staring point
        d (double[3]): the direction of the ray
        tris (Triangle*): the array of triangles
        num_tris (int): the length of the give array
        body_inclusion (set[int]&): the set of bodies containing the starting point
    
    Outputs:
        intersected_tri (Triangle**): intersected_tri to be updated to the pointer to the intersected triangle.
    Returns:
        double : distance to the nearest intersecting triangle
        
    Notes:
    Sven Woop, Carten Benthin, and Ingo Wald, Journal of Computer Graphics Techniques Vol. 2 65 (2013)
    """
    cdef int kx, ky, kz, i
    cdef double Ax, Ay, Az, Bx, By, Bz, Cx, Cy, Cz
    cdef double u, v, w, det, ds, t, min_ds
    cdef double a[3]
    cdef double b[3]
    cdef double c[3]
    cdef Triangle* tri

    min_ds = 1.0e31
    intersected_tri[0] = NULL

    orient_along_max_dim(d, &kx, &ky, &kz)

    sx = d[kx] / d[kz]
    sy = d[ky] / d[kz]
    sz = 1.0 / d[kz]

    # Check intersection against every triangles for the current octree node.
    for i in range(num_tris):
        tri = tris + i

        # only check against a right kind of triangles (either inside out outside).
        # When the particle is outside(or inside) of a body (ibody),
        # it checks only against  outside(or inside) triangles
        if tri.is_outside != (body_inclusion.find(tri.body) == body_inclusion.end()):
            continue

        subtract(a, tri.a, o)
        subtract(b, tri.b, o)
        subtract(c, tri.c, o)

        Ax = a[kx] - sx * a[kz]
        Ay = a[ky] - sy * a[kz]
        Bx = b[kx] - sx * b[kz]
        By = b[ky] - sy * b[kz]
        Cx = c[kx] - sx * c[kz]
        Cy = c[ky] - sy * c[kz]

        u = Cx * By - Cy * Bx
        v = Ax * Cy - Ay * Cx
        w = Bx * Ay - By * Ax

        det = u + v + w

        if (u < 0 or v < 0 or w < 0) and (u > 0 or v > 0 or w > 0):
            continue

        if det == 0:
            continue

        Az = sz * a[kz]
        Bz = sz * b[kz]
        Cz = sz * c[kz]
        t = u * Az + v * Bz + w * Cz
        ds = t / det

        if ds < -1.0e-15 or ds > min_ds:
            continue
        else:
            min_ds = ds
            intersected_tri[0] = tri

    return min_ds


cdef class TriangleWrap:
    """This is a wrapper class for Triangle struct

    This behaves something like a value class.
    It stores the copy of a triangle given to the factory method from_ptr.
    """
    @staticmethod
    cdef cnp.ndarray[cnp.float64_t, ndim=1] pd_to_ndarray(double[:] d):
        cdef cnp.ndarray[cnp.float64_t, ndim=1] v
        v = np.zeros(shape=(3,), dtype=np.float64)
        v[:] = d
        return v

    @property
    def a(self):
        return TriangleWrap.pd_to_ndarray(self.tri.a)

    @property
    def b(self):
        return TriangleWrap.pd_to_ndarray(self.tri.b)

    @property
    def c(self):
        return TriangleWrap.pd_to_ndarray(self.tri.c)

    @property
    def body(self):
        return self.tri.body

    @property
    def is_outside(self):
        return self.tri.is_outside

    @staticmethod
    cdef TriangleWrap from_ptr(Triangle* tri):
        t = TriangleWrap()
        # It stores a copy the given Triangle* tri.
        t.tri = tri[0]
        return t

    def __repr__(self):
        return "Triangle a({0:.3e}, {1:.3e}, {2:.3e})" \
               " b({3:.3e}, {4:.3e}, {5:.3e})" \
               " c({6:.3e}, {7:.3e} {8:.3e}) body({9:d}) is_outside({10:b})" \
            .format(self.tri.a[0], self.tri.a[1], self.tri.a[2],
                    self.tri.b[0], self.tri.b[1], self.tri.b[2],
                    self.tri.c[0], self.tri.c[1], self.tri.c[2],
                    self.tri.body, self.tri.is_outside)
