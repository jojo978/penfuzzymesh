# distutils: language = c++

from libc.stdlib cimport malloc, free
from libc.stdio cimport printf
from libc.math cimport fabs, sqrt
from libc.string cimport memset

from libcpp.set cimport set
from libcpp.pair cimport pair
from libcpp.limits cimport numeric_limits
from cython.operator cimport dereference as deref
from cython.operator cimport postincrement

import numpy as np

from .octree cimport *
from .triangle cimport *


cdef bint is_separable_along_axis_against_unit_cube(double* a, double* va, double* vb, double* vc):
    """Test if a triangle is separable from an unit cube along a given test axis.
    1x1x1 cube is assumed to be centered at the origin.
    
    Args:
        a (double[3]): test axis
        va (double[3]): the first vertex of the triangle
        vb (double[3]): the second vertex of the triangle
        vc (double[3]): the third vertex of the triangle
        
    Returns:
        bint: True when the triangle and the unit cube can be separated by a plane,
                which is normal to the axis a. False otherwise.
    """
    cdef double pmin, pmax, c, r

    pmin = pmax = c = dot_product(a, va)
    c = dot_product(a, vb)
    pmin = pmin if pmin < c else c
    pmax = pmax if pmax > c else c
    c = dot_product(a, vc)
    pmin = pmin if pmin < c else c
    pmax = pmax if pmax > c else c

    r = (fabs(a[0]) + fabs(a[1]) + fabs(a[2])) / 2

    return (pmin > r) or (pmax < -r)


def is_triangle_aabb_overlap(double[:] c, double[:] h, tri):
    """Test the overlap between an AABB (Axis Aligned Bounding Box) and a triangle
        This function is a python binding for is_triangle_aabb_overlap_

    Args:
        c(double[:]) : the center of the aabb
        h(double[:]) : the sides of the aabb
        tri : The triangle to be tested

    Returns:
        bool : False when they are separable, True when they overlap
    """
    cdef Triangle triangle
    cdef bint is_overlap

    init_triangle(&triangle, tri)
    is_overlap = is_triangle_aabb_overlap_(&c[0], &h[0], &triangle)
    return is_overlap


cdef bint is_triangle_aabb_overlap_(double* c, double* h, Triangle* tri):
    """Test the overlap between an AABB (Axis Aligned Bounding Box) and a triangle.
    Exhaustive tests along all 14 axes
    
    Args:
        c (double[3]): the center of the aabb, length of 3
        h (double[3]): the sides of the aabb, length of 3
        tri (Triangle*): The triangle to be tested

    Returns:
        bool : False when they are separable, True when they overlap
    """
    cdef:
        vec3 tva, tvb, tvc, tnormal, axis

        double ce[3][3]
        double te[3][3]

        double* p[4]
        double* q[4]

        int i, j

    # A given box (c, h) is transformed to a unit cube.
    # And the triangle and its normal are also transformed accordingly.
    p = (tri.a, tri.b, tri.c, tri.normal)
    q = (&tva[0], &tvb[0], &tvc[0], &tnormal[0])

    for i in range(4):
        for j in range(3):
            q[i][j] = (p[i][j] - c[j]) / h[j]

    #  3 edges of the unit cube.
    ce[0] = (1, 0, 0)
    ce[1] = (0, 1, 0)
    ce[2] = (0, 0, 1)

    # 3 tests along ce[i]
    for i in range(3):
        if is_separable_along_axis_against_unit_cube(ce[i], tva, tvb, tvc):
            return False

    # 1 test along the normal of the triangle
    if is_separable_along_axis_against_unit_cube(tnormal, tva, tvb, tvc):
        return False

    # 3 triangle edges
    subtract(te[0], tvb, tva)
    subtract(te[1], tvc, tvb)
    subtract(te[2], tva, tvc)

    # 9 tests along ce[i] x te[j]
    for i in range(3):
        for j in range(3):
            cross_product(axis, ce[i], te[j])
            if is_separable_along_axis_against_unit_cube(axis, tva, tvb, tvc):
                return False

    # all(14) tests failed, they must be overlapped.
    return True


cdef Node* new_node(int level):
    """Create a new node
    
    Initializes it to zero.
    
    Args:
        level (int):
    """
    cdef Node* node
    node = <Node*>malloc(sizeof(Node))
    node.level = level

    node.tris = NULL
    node.num_tris = 0

    memset(node.children_, 0, sizeof(node.children_))
    memset(node.neighbors_, 0, sizeof(node.neighbors_))

    return node


cdef void free_node(Node* node):
    cdef int i

    # If node is not fully initialized, both of node.tris and node.children_[*] are NULL.
    free(node.tris)

    for i in range(8):
        if node.children_[i] != NULL:
            free_node(node.children_[i])

    free(node)


cdef int build_tree(Node* node, Triangle** tris, int num_tris):
    """Build octree nodes
    
    It assumes that node (Node*) is allocated but in a clean state.
    
    This function doesn't take ownerships to provided triangles.
    It is the caller's responsibility to properly destroy those triangles after calling build_tree.
    This function instead copies provided triangles and store them in octree (sub) nodes and takes ownership to those copies.
    
    Args:
        node (Node*): the root node for construction.
        tris (Triangle**): array of Triangles
        num_tris (int): the number of Triangles
    
    Returns:
        int: the number of nodes including the root node and its sub nodes.
    """
    cdef:
        int i, j, sub_num_tris
        int num_sub_nodes
        Triangle** tri_grps
        Node* child
        double c[3]
        double h[3]

    # the root node counts.
    num_sub_nodes = 1

    # it's a node splitting condition. node.level is bounded above by 7.
    if num_tris > 7 > node.level:
        # non-leaf node, splitting this node and build_tree recursively

        # sides of the children nodes with a small padding
        # Actual sub node sizes won't have any padding.
        # This padding are for triangle and node (conservative) intersection tests.
        h = ((node.xmax - node.xmin) / 2 + 1e-10,
             (node.ymax - node.ymin) / 2 + 1e-10,
             (node.zmax - node.zmin) / 2 + 1e-10)

        # temporary array of pointers to Triangle
        # This array will be destroyed after all its sub nodes are built.
        # Triangles themselves are not destroyed by this function. The caller should handle that.
        tri_grps = <Triangle**>malloc(sizeof(Triangle*) * num_tris)

        for i in range(8):
            child = new_node(node.level + 1)
            node.children_[i] = child

            if i < 4:
                child.zmin = node.zmin
                child.zmax = (node.zmin + node.zmax) / 2
            else:
                child.zmin = (node.zmin + node.zmax) / 2
                child.zmax = node.zmax

            if (i // 2) % 2 == 0:
                child.ymin = node.ymin
                child.ymax = (node.ymin + node.ymax) / 2
            else:
                child.ymin = (node.ymin + node.ymax) / 2
                child.ymax = node.ymax

            if i  % 2 == 0:
                child.xmin = node.xmin
                child.xmax = (node.xmin + node.xmax) / 2
            else:
                child.xmin = (node.xmin + node.xmax) / 2
                child.xmax = node.xmax

            # c(double[3]) is the center of the children node
            c = ((child.xmin + child.xmax) / 2,
                 (child.ymin + child.ymax) / 2,
                 (child.zmin + child.zmax) / 2)

            sub_num_tris = 0

            for j in range(num_tris):
                if is_triangle_aabb_overlap_(c, h, tris[j]):
                    tri_grps[sub_num_tris] = tris[j]
                    sub_num_tris += 1

            # even when sub_num_tris is zero, build_tree needs to be called.
            num_sub_nodes += build_tree(child, tri_grps, sub_num_tris)

        # destroy the array of pointers to Triangles. Not triangles themselves
        free(tri_grps)
    else:
        # Triangles are copied to a contiguous memory of this node.
        if num_tris > 0:
            node.tris = <Triangle*>malloc(sizeof(Triangle) * num_tris)
            node.num_tris = num_tris

            for i in range(num_tris):
                # Triangle is copied here.
                node.tris[i] = tris[i][0]

    return num_sub_nodes


cdef Node* find_node_fast(Node* node, double x, double y, double z, int level=-1):
    """Find the octree node that contains the point with the level=level
    
    It assumes that (x, y, z) is contained in the starting node.
    Also, it assume that level is smaller the level of the starting node.
    
    Args:
        node (Node*): the starting node
        x (double)
        y (double)
        z (double)
        level (int): target octree node level, -1 indicates leaf node
    
    Returns:
        Node* : The octree node that contains the point (x, y, z)
    """
    cdef:
        int num_child

    # If it's the leaf node.
    if node.children_[0] == NULL:
        return node

    # It only searches up to the level "level".
    if node.level == level:
        return node

    num_child = 7

    if x < (node.xmin + node.xmax) / 2:
        num_child -= 1
    if y < (node.ymin + node.ymax) / 2:
        num_child -= 2
    if z < (node.zmin + node.zmax) / 2:
        num_child -= 4

    return find_node_fast(node.children_[num_child], x, y, z, level=level)


cdef Node* find_node(Node* node, double x, double y, double z, int level=-1):
    """Similar to find_node_fast but it makes sure that (x, y, z) is contained in the starting node.
    If not, it returns NULL.
    """
    if x < node.xmin or x > node.xmax:
        return NULL

    if y < node.ymin or y > node.ymax:
        return NULL

    if z < node.zmin or z > node.zmax:
        return NULL

    return find_node_fast(node, x, y, z, level)


cdef void set_neighbors(Node* node, Node* root_node):
    """Initializes neighbor nodes for a given node.
    
    Only leaf nodes get non-zero neighbors.
    the level of neighbor nodes will be smaller than or equal to the level of the node.
    
    Args:
        node (Node*): all of leaf nodes of this node are provided references to neighbors nodes.
        root_node (Node*): the root node of the octree.
     
    neighbor codes
    0 -> +X
    1 -> -X
    2 -> +Y
    3 -> -Y
    4 -> +Z
    5 -> -Z
    """
    cdef:
        int i
        double eps = 1.0e-13
        double x, y, z

    if node.children_[0] != NULL:
        # it's not a leaf node.
        for i in range(8):
            set_neighbors(node.children_[i], root_node)
    else:
        x = node.xmax + eps
        y = (node.ymax + node.ymin) * 0.5
        z = (node.zmax + node.zmin) * 0.5
        node.neighbors_[0] = find_node(root_node, x, y, z, level=node.level)

        x = node.xmin - eps
        node.neighbors_[1] = find_node(root_node, x, y, z, level=node.level)

        x = (node.xmax + node.xmin) * 0.5
        y = node.ymax + eps
        node.neighbors_[2] = find_node(root_node, x, y, z, level=node.level)

        y = node.ymin - eps
        node.neighbors_[3] = find_node(root_node, x, y, z, level=node.level)

        y = (node.ymax + node.ymin) * 0.5
        z = node.zmax + eps
        node.neighbors_[4] = find_node(root_node, x, y, z, level=node.level)

        z = node.zmin - eps
        node.neighbors_[5] = find_node(root_node, x, y, z, level=node.level)


cdef inline void move_particle(double* x, double* y, double* z, double u, double v, double w, double ds):
    """Move the particle by ds along (u, v, w)
    
    Args:
        x (double*)
        y (double*)
        z (double*)
        u (double)
        v (double)
        w (double)
        ds (double): distance to move
    """
    x[0] += u * ds
    y[0] += v * ds
    z[0] += w * ds


cdef void get_node_edges(Node* node, vertices, edges):
    """This function might be useful for visualizing the octree in the wire frame mode
    """
    cdef int i, j, k, l, lvs

    if node.children_[0] != NULL:
        vs = vertice_of_node(node)
        vs = np.asarray(vs)

        es = [[0, 1], [1, 2], [2, 3], [3, 0],
              [4, 5], [5, 6], [6, 7], [7, 4],
              [0, 4], [1, 5], [2, 6], [3, 7]]  # 12 vertices

        # nvs has 18 new vertices
        nvs = [(vs[i] + vs[j]) / 2 for i, j in es] + \
              [[node.xmin, (node.ymin + node.ymax) / 2, (node.zmin + node.zmax) / 2],
               [node.xmax, (node.ymin + node.ymax) / 2, (node.zmin + node.zmax) / 2],
               [(node.xmin + node.xmax) / 2, node.ymin, (node.zmin + node.zmax) / 2],
               [(node.xmin + node.xmax) / 2, node.ymax, (node.zmin + node.zmax) / 2],
               [(node.xmin + node.xmax) / 2, (node.ymin + node.ymax) / 2, node.zmin],
               [(node.xmin + node.xmax) / 2, (node.ymin + node.ymax) / 2, node.zmax]]

        lvs = len(vertices)

        # 15 edges
        nes = []
        for i, j, k, l in [[0, 1, 2, 3], [4, 5, 6, 7], [0, 9, 4, 8], [1, 10, 5, 9], [2, 11, 6, 10], [3, 8, 7, 11]]:
            nes.extend([[lvs + i, lvs + k], [lvs + j, lvs + l]])
        nes.extend([[lvs + i, lvs + i + 1] for i in range(12, 18, 2)])

        vertices.extend(nvs)
        edges.extend(nes)

        for i in range(8):
            get_node_edges(node.children_[i], vertices, edges)


cdef class NodeWrap:
    """Python wrapper for struct Node.

    This class doesn't own the node. It only has a reference to it.
    Once the node, which is pointed to by this class, is freed,
    Using NodeWrap afterwards may cause a segmentation fault.
    """
    @staticmethod
    cdef NodeWrap from_ptr(Node* ptr):
        n = NodeWrap()
        n.pnode = ptr
        return n

    @property
    def bounds(self):
        cdef Node* n = self.pnode
        return n.xmin, n.ymin, n.zmin, n.xmax, n.ymax, n.zmax

    @property
    def level(self):
        return self.pnode.level

    @property
    def num_tris(self):
        return self.pnode.num_tris

    @property
    def children(self):
        cdef Node* p
        if self.pnode.children_[0] != NULL:
            return [NodeWrap.from_ptr(p) for p in self.pnode.children_]
        else:
            return []

    @property
    def neighbors(self):
        cdef Node* p
        return [NodeWrap.from_ptr(p) if p != NULL else None for p in self.pnode.neighbors_]

    def __repr__(self):
        cdef Node* p = self.pnode
        return "bounds : ({0:.3e}, {1:.3e}, {2:.3e}, {3:.3e}, {4:.3e}, {5:.3e}), level : {6:d}".format(
            p.xmin, p.ymin, p.zmin, p.xmax, p.ymax, p.zmax, p.level)


cdef vertice_of_node(Node* node):
    return [(node.xmin, node.ymin, node.zmin),
            (node.xmax, node.ymin, node.zmin),
            (node.xmax, node.ymax, node.zmin),
            (node.xmin, node.ymax, node.zmin),
            (node.xmin, node.ymin, node.zmax),
            (node.xmax, node.ymin, node.zmax),
            (node.xmax, node.ymax, node.zmax),
            (node.xmin, node.ymax, node.zmax)]


cdef class Octree:
    def __cinit__(self, maxlevel, body_mat_map, body_dsmax_map):
        """
        Args:
            maxlevel (int): maximum level of Octree (not implemented yet. maximum level is 7 always for now)
            body_mat_map (set[int, int]):
            body_dsmax_map (set[int, double]):
        """
        cdef int body, mat

        self.root = new_node(0)
        self.node = self.root
        self.maxlevel = maxlevel
        self.mats = body_mat_map
        self.ds_maxs = body_dsmax_map
        self.mats[0] = 0

    def __dealloc__(self):
        free_node(self.root)

    def get_bounds(self):
        cdef Node* node = self.root
        return (node.xmin, node.ymin, node.zmin), (node.xmax, node.ymax, node.zmax)

    def set_bounds(self, double xmin, double ymin, double zmin, double xmax, double ymax, double zmax):
        self.root.xmin = xmin
        self.root.ymin = ymin
        self.root.zmin = zmin
        self.root.xmax = xmax
        self.root.ymax = ymax
        self.root.zmax = zmax

    def find_node(self, x, y, z, level=-1):
        self.node = find_node(self.root, x, y, z, level=level)
        return NodeWrap.from_ptr(self.node)

    def get_node_bounds(self):
        cdef Node* node
        node = self.node
        return node.xmin, node.ymin, node.zmin, node.xmax, node.ymax, node.zmax

    def get_node_level(self):
        return self.node.level

    def build(self, triangles):
        """Build octree with a given list of triangles

        Args:
            triangles (list of [va(double[3]), vb(double[3]), vc(double[3]), body(int), is_outside(bint)]):
        """
        cdef:
            Triangle** tris
            Triangle* t
            int i

        tris = <Triangle**>malloc(sizeof(Triangle*) * len(triangles))

        for i, triangle in enumerate(triangles):
            tris[i] = new_triangle(triangle)
            t = tris[i]
            self.body_meshes[t.body].push_back(t[0])

        self.num_nodes_ = build_tree(self.root, tris, len(triangles))

        for i in range(len(triangles)):
            free(tris[i])

        free(tris)
        set_neighbors(self.root, self.root)

    @property
    def body_inclusion(self):
        """Pyton interface to self.body_inclusion_
        """
        return self.body_inclusion_

    @body_inclusion.setter
    def body_inclusion(self, bi):
        self.body_inclusion_ = bi
    
    @property
    def num_nodes(self):
        return self.num_nodes_

    def get_edges(self):
        vertices = vertice_of_node(self.root)
        edges = [[0, 1], [1, 2], [2, 3], [3, 0], [4, 5], [5, 6], [6, 7], [7, 4], [0, 4], [1, 5], [2, 6], [3, 7]]

        get_node_edges(self.root, vertices, edges)
        return vertices, edges

    def ds_wall(self, x, y, z, u, v, w):
        """Python interface to self.ds_wall_
        """
        cdef double ds
        cdef int neighbor

        ds = self.ds_wall_(x, y, z, u, v, w, &neighbor)

        return ds, neighbor

    cdef double ds_wall_(self, double x, double y, double z, double u, double v, double w, int* num_neighbor):
        """Distance to the bounding surface of the current AABB node along (u, v, w).
        
        It assumes that (x, y, z) is contained in self.node.
        self.find_node sets self.node tp be consistent with a given point.
        
        Args:
            x (double)
            y (double)
            z (double)
            u (double)
            v (double)
            w (double)
            num_neighbor (int*): [OUT] codes for the crossing face of AABB.
        
        Returns:
            double : distance to that face of AABB along (u, v, w)
        """
        cdef double ds, temp

        ds = numeric_limits[double].max()

        if u > 0:
            ds = (self.node.xmax - x) / u
            num_neighbor[0] = 0
        elif u < 0:
            ds = (self.node.xmin - x) / u
            num_neighbor[0] = 1

        if v > 0:
            temp = (self.node.ymax - y) / v
            if temp < ds:
                ds = temp
                num_neighbor[0] = 2
        elif v < 0:
            temp = (self.node.ymin - y) / v
            if temp < ds:
                ds = temp
                num_neighbor[0] = 3

        if w > 0:
            temp = (self.node.zmax - z) / w
            if temp < ds:
                ds = temp
                num_neighbor[0] = 4
        elif w < 0:
            temp = (self.node.zmin - z) / w
            if temp < ds:
                ds = temp
                num_neighbor[0] = 5

        return ds

    cpdef int get_current_body(self):
        """Only looks up body_flag
        """
        if self.body_inclusion_.empty():
            return 0
        else:
            # takes the largest ibody in self.body_inclusion_
            return deref(self.body_inclusion_.rbegin())

    def get_root_node(self):
        return NodeWrap.from_ptr(self.root)

    def get_node(self):
        return NodeWrap.from_ptr(self.node)
    
    def set_node(self, NodeWrap node):
        self.node = node.pnode

    def get_triangles(self):
        """Get all triangles intersecting the current octree node

        Returns:
            list[TriangleWrap] or None
        """
        cdef int i

        if self.node.num_tris > 0:
            tris = []
            for i in range(self.node.num_tris):
                tris.append(TriangleWrap.from_ptr(self.node.tris + i))

            return tris

    def ds_triangle(self, double[:] oa, double[:] da):
        """Python wrapper for ds_triangle_

        Args:
            oa(double[:] or compatible): [IN] length of 3
            da(double[:] or compatible): [IN] length of 3

        Returns:
            (float, TriangleWrap): if the ray intersects with a triangle
        """
        cdef:
            double* o
            double* d
            double ds
            Triangle* tri

        # o, d = &oa[0], &da[0]
        # ds = self.ds_triangle_(o, d, &tri)
        ds = self.ds_triangle_(&oa[0], &da[0], &tri)

        if tri != NULL:
            return ds, TriangleWrap.from_ptr(tri)

    cdef inline double ds_triangle_(self, double* o, double* d, Triangle** intersected_tri):
        """Calculate the shortest distance to any of triangles of the current octree node.
                
        Args:
            o (double[3]): [IN] the starting point. This point should be inside of the current node
            d (double[3]): [IN] the direction of the ray
            intersected_tri (Triangle**): [OUT] updated to the pointer to the intersected triangle unless NULL
        
        Returns:
            double : the shortest distance to a triangle
                
        Journal of Computer Graphics Techniques
        Watertight Ray/Triangle Intersection Vol. 2, No. 1, 2013 http://jcgt.org
        Sven Woop, Carsten Benthin, Ingo Wald
        """
        cdef double min_ds

        min_ds = numeric_limits[double].max()
        intersected_tri[0] = NULL

        if self.node.num_tris == 0:
            return min_ds

        min_ds = wbw_intersect_(o, d, self.node.tris, self.node.num_tris, self.body_inclusion_, intersected_tri)

        return min_ds

    def locate(self, double x, double y, double z):
        """Python wrapper for locate_
        """
        cdef int body, mat

        self.locate_(x, y, z, &body, &mat)

        return body, mat

    cdef void locate_(self, double x, double y, double z, int* pb, int* pmat):
        """Determine body and mat for (x, y, z)
        
        It sets self.node to the node containing (x, y z)
        It updates self.body_inclusion_. This set[int] will include all bodies containing (x, y, z).
        
        Args:
            x (double): [IN]
            y (double): [IN]
            z (double): [IN]
            pb (double*): [OUT] body for (x, y, z)
            pmat (double*): [OUT] mat for (x, y, z)
        """
        cdef Triangle tri
        cdef vector[Triangle] tris
        cdef double o[3]
        cdef double dist

        # nearest_triangles stores the nearest triangle for each body
        cdef map[int,pair[double,Triangle]] nearest_triangles
        cdef map[int,vector[Triangle]].iterator it_body_tris
        cdef map[int,pair[double,Triangle]].iterator it

        self.node = find_node(self.root, x, y, z)

        o = x, y, z
        self.body_inclusion_.clear()

        it_body_tris = self.body_meshes.begin()
        while it_body_tris != self.body_meshes.end():
            for tri in deref(it_body_tris).second:
                dist = distance_to_triangle_(o, &tri)
                it = nearest_triangles.find(tri.body)
                if it != nearest_triangles.end():
                    # There was a triangle of tri.body
                    if deref(it).second.first > dist:
                        # if dist is smaller than the smallest of earlier distances.
                        deref(it).second.first = dist
                        deref(it).second.second = tri
                else:
                    # There has not been any triangle of tri.body
                    nearest_triangles[tri.body] = pair[double,Triangle](dist, tri)

            # all triangles for a body (=deref(it_body_tris).first) are visited.
            # And nearest_triangles must have a key (tri.body) here.
            if nearest_triangles[tri.body].second.is_outside:
                self.body_inclusion_.insert(deref(it).first)

            postincrement(it_body_tris)

        pb[0] = self.get_current_body()
        pmat[0] = self.mats[pb[0]]

    def step(self, double x, double y, double z, double u, double v, double w, double ds):
        """Python interface to step_
        """
        cdef double dsef
        cdef int body, mat, ncross

        body = self.get_current_body()
        mat = self.mats[body]

        ncross = self.step_(&x, &y, &z, u, v, w, ds, &dsef, &body, &mat)

        return ncross, x, y, z, dsef, body, mat

    cdef int step_(self, double* x, double* y, double* z,
                   double u, double v, double w, double ds, double* dsef, int* pb, int* pm):
        """This method tries to move the particle by ds along (u, v, w).
        
        It moves the particle by full ds if the particle always stays in the same material along the way.
        If the particle encounters the material changing boundary, it stops there.
        But, if the new material is 0 (i.e. vacuum), it will precede further until it sees non-zero material or escapes the domain.
        
        It assumes that pb[0] and pm[0] are right body and materials for (x[0], y[0], z[0]) before calling this method.
        self.body_inclusion_ (set[int]) also needs to be consistent with (x[0], y[0], z[0]) initially.
        All will stay in consistent states after this method. 
        
        Args:
            x(double*): [IN/OUT] the x coordinate of the particle, which is updated to the final position
            y(double*): [IN/OUT] the y coordinate of the particle
            z(double*): [IN/OUT] the z coordinate of the particle
            u (double): [IN] the u component direction of the ray
            v (double): [IN] the v component direction of the ray
            w (double): [IN] the w component direction of the ray
            ds (double): [IN] the requested distance to travel
            dsef(double*): [OUT] actual travel length in non-zero material
            pb(double*): [IN/OUT] the containing body of the particle
            pm(double*): [IN/OUT] the material of the final position
            
        Return:
            int : the number of interface crossings.
        """
        cdef:
            int num_neighbor, body, mat, ncross, new_mat
            double ds_wall, ds_triangle, ds_remaining
            double o[3]
            double d[3]
            Triangle* tri
            set[int].iterator it
            set[int]* body_inclusion = &self.body_inclusion_

        ds_remaining = ds
        dsef[0] = 0
        ncross = 0
        body = pb[0]
        mat = pm[0]

        d = u, v, w

        while True:
            o = x[0], y[0], z[0]

            ds_wall = self.ds_wall_(x[0], y[0], z[0], u, v, w, &num_neighbor)
            ds_triangle = self.ds_triangle_(o, d, &tri)

            if mat == 0:
                # here, mat is zero. no interaction happens here. So ds_remaining is irrelevant.
                # Proceeds until you see a body or reaches outside of the octree.
                if ds_wall < ds_triangle:
                    move_particle(x, y, z, u, v, w, ds_wall)

                    if self.node.neighbors_[num_neighbor] != NULL:
                        # continue with a new node
                        self.node = find_node_fast(self.node.neighbors_[num_neighbor], x[0], y[0], z[0])
                    else:
                        # it's escaping the octree, Thus, stop here.
                        # pm[0], pb[0] must be already 0 here.
                        break
                else:
                    move_particle(x, y, z, u, v, w, ds_triangle)
                    ncross += 1
                    # The particle just crossed the body (ibody=tri.body) surface.
                    # update body_inclusion, body, and mat
                    it = body_inclusion.find(tri.body)
                    if it == body_inclusion.end():
                        body_inclusion.insert(tri.body)
                    else:
                        body_inclusion.erase(it)

                    pb[0] = self.get_current_body()
                    pm[0] = self.mats[pb[0]]  # pm[0] must be non-zero.

                    # mat is changed from 0 to non-zero (pm[0] != 0). Thus, stop here.
                    break
            else:
                # here, material is non zero.
                if ds_remaining < ds_wall and ds_remaining < ds_triangle:
                    # The particle doesn't cross any octree node walls or body surfaces before ds_remaining
                    dsef[0] += ds_remaining

                    move_particle(x, y, z, u, v, w, ds_remaining)

                    # mat and body don't changed either.
                    break
                elif ds_wall < ds_triangle:
                    # The particle crossed an octree node boundary.
                    dsef[0] += ds_wall
                    ds_remaining -= ds_wall

                    move_particle(x, y, z, u, v, w, ds_wall)

                    if self.node.neighbors_[num_neighbor] != NULL:
                        # continue with a new node
                        # note that self.node_neighbors_[num_neighbor] may not be a leaf node.
                        self.node = find_node_fast(self.node.neighbors_[num_neighbor], x[0], y[0], z[0])
                    else:
                        # it's escaping the octree. Stop here.
                        break
                else:
                    # the particle crossed a body surface.
                    # note that body and material could remain the same though.
                    # but body_inclusion will be changed definitely.
                    dsef[0] += ds_triangle
                    ds_remaining -= ds_triangle
                    ncross += 1

                    move_particle(x, y, z, u, v, w, ds_triangle)

                    # update flight log
                    # it = body_inclusion.find(tri.body)
                    # if it == body_inclusion.end():
                    #     body_inclusion.insert(tri.body)
                    # else:
                    #     body_inclusion.erase(it)
                    if tri.is_outside:
                        # it touches a mesh used when the particle
                        # is outside. so, it's moving in.
                        body_inclusion.insert(tri.body)
                    else:
                        # it touches a mesh used when the particle
                        # is inside. so, it's moving out. 
                        it = body_inclusion.find(tri.body)
                        if it != body_inclusion.end():
                            body_inclusion.erase(it)

                    # pb[0] is set to a new ibody (though this new ibody could be same with old body)
                    pb[0] = self.get_current_body()
                    mat = self.mats[pb[0]]

                    # if mat(mat of new body) is different from pm[0](old mat) and is non-zero, stop!
                    # otherwise continue with the same mat or zero mat
                    if mat != 0 and pm[0] != mat:
                        pm[0] = mat
                        break
                    else:
                        pm[0] = mat

        return ncross
