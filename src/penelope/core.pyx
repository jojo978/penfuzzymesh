# distutils: language = c++
from enum import IntEnum

from libc.stdio cimport printf
from libc.string cimport strcpy, memset
from libc.math cimport sqrt, cos, sin, isnan

import numpy as np

from pandas import DataFrame, Series, MultiIndex

from . cimport core
from . cimport octree

from .core cimport (__track_mod_MOD_e as track_e,
                    __track_mod_MOD_x as track_x,
                    __track_mod_MOD_y as track_y,
                    __track_mod_MOD_z as track_z,
                    __track_mod_MOD_u as track_u,
                    __track_mod_MOD_v as track_v,
                    __track_mod_MOD_w as track_w,
                    __track_mod_MOD_wght as track_wght,
                    __track_mod_MOD_ibody as track_ibody,
                    __track_mod_MOD_mat as track_mat,
                    __track_mod_MOD_kpar as track_kpar,
                    __track_mod_MOD_ilb as track_ilb,
                    __track_mod_MOD_ipol as track_ipol,
                    __track_mod_MOD_e0segm as track_e0segm,
                    __track_mod_MOD_desoft as track_desoft,
                    __track_mod_MOD_ssoft as track_ssoft)

from .core cimport (__penelope_mod_MOD_eabs as penelope_eabs,
                    __penelope_mod_MOD_c1 as penelope_c1,
                    __penelope_mod_MOD_c2 as penelope_c2,
                    __penelope_mod_MOD_wcc as penelope_wcc,
                    __penelope_mod_MOD_wcr as penelope_wcr)

from .core cimport __penvared_mod_MOD_force as penvared_force


class Par(IntEnum):
    electron = 1
    photon = 2
    positron = 3


cdef class DoubleAttr:
    @staticmethod
    cdef DoubleAttr create(double *p):
        v = DoubleAttr()
        v.p = p
        return v

    def __get__(self, instance, owner):
        return self.p[0]

    def __set__(self, instance, value):
        self.p[0] = value


cdef class IntAttr:
    @staticmethod
    cdef IntAttr create(int *p):
        v = IntAttr()
        v.p = p
        return v

    def __get__(self, instance, owner):
        return self.p[0]

    def __set__(self, instance, value):
        self.p[0] = value


cdef class Track:
    """Python interface to PENELOPE Track_mod variables

    This class stores the pointer to module variables.
    """
    x = DoubleAttr.create(&track_x)
    y = DoubleAttr.create(&track_y)
    z = DoubleAttr.create(&track_z)
    e = DoubleAttr.create(&track_e)

    u = DoubleAttr.create(&track_u)
    v = DoubleAttr.create(&track_v)
    w = DoubleAttr.create(&track_w)

    wght = DoubleAttr.create(&track_wght)

    kpar = IntAttr.create(&track_kpar)
    ibody = IntAttr.create(&track_ibody)
    mat = IntAttr.create(&track_mat)
    ipol = IntAttr.create(&track_ipol)

    @property
    def ilb(self):
        """It wraps int track_ilb[5] as a numpy array.

        A returned numpy array doesn't own the memory.

        Returns:
            ndarray: length of 5, dtype=np.int32
        """
        cdef int[:] ilb
        ilb = track_ilb
        return np.asarray(ilb)

    @ilb.setter
    def ilb(self, l):
        """It copies the content of a given argument into track_ilb
        
        Args:
            l: iterable of 5 ints
        """
        cdef int i, x
        for i, x in enumerate(l):
            track_ilb[i] = x

    @property
    def e0segm(self):
        return track_e0segm

    @property
    def desoft(self):
        return track_desoft

    @property
    def ssoft(self):
        return track_ssoft


class Penelope:
    @property
    def eabs(self):
        cdef:
            double[:, :] eabs = penelope_eabs
        d = np.asarray(eabs)
        df = DataFrame(d, columns=['electron', 'photon', 'positron'], index=np.arange(1, d.shape[0] + 1))
        df.index.name = 'mat'
        return df
    
    @property
    def c1(self):
        cdef double[:] c1 = penelope_c1
        s = Series(c1, index=np.arange(1, c1.shape[0] + 1))
        s.index.name = 'mat'
        return s
    
    @property
    def c2(self):
        cdef double[:] c2 = penelope_c2
        s = Series(c2, index=np.arange(1, c2.shape[0] + 1))
        s.index.name = 'mat'
        return s
    
    @property
    def wcc(self):
        cdef double[:] wcc = penelope_wcc
        s = Series(wcc, index=np.arange(1, wcc.shape[0] + 1))
        s.index.name = 'mat'
        return s
    
    @property
    def wcr(self):
        cdef double[:] wcr = penelope_wcr
        s = Series(wcr, index=np.arange(1, wcr.shape[0] + 1))
        s.index.name = 'mat'
        return s


class Penvared:
    @property
    def force(self):
        cdef:
            double [:, :, :] force = penvared_force
            aforce = np.asarray(force)
            icols = range(1, 9)  # ICOL = 1, ..., 8
            columns = MultiIndex.from_product([[Par.electron, Par.photon, Par.positron], range(1, NBV + 1)])
            df = DataFrame(aforce.reshape(8, -1), index=icols, columns=columns)

        return df


def peinit(max_energy, mat_files, info_level=1):
    cdef:
        int i, nmatter
        double emax
        int iwr = 6
        int info = 1
        char pmfiles [MAXMAT][64]

    emax = max_energy
    nmatter = len(mat_files)
    info = info_level

    if nmatter > MAXMAT:
        raise ValueError("the number of materials is greater than MAXMAT.")

    memset(pmfiles, 0, sizeof(pmfiles) * sizeof(char))

    for i, fn in enumerate(mat_files):
        strcpy(pmfiles[i], fn.encode())
        # for kpar in range(3):
        #     penelope_eabs[i][kpar] = eabs

    core.peinit_(&emax, &nmatter, &iwr, &info, &pmfiles[0][0])


def cleans():
    core.cleans_()


def stores(double e, double x, double y, double z,
           double u, double v, double w, double wght, int kpar, int[:] ilba, int ipol):
    """Push one particle into the secondary particle stack.

    ilba is assumed to be like a numpy array with contiguous memory and dtype=np.int32
    """
    core.stores_(&e, &x, &y, &z, &u, &v, &w, &wght, &kpar, &ilba[0], &ipol)


def start():
    core.start_()


def secpar():
    """Pop one particle from the secondary particle stack.

    """
    cdef:
        int left

    core.secpar_(&left)

    return left


def jump(dsmax):
    cdef:
        double ds_max
        double ds

    ds_max = dsmax
    core.jump_(&ds_max, &ds)

    return ds


def jumpf(dsmax):
    cdef:
        double ds_max
        double ds

    ds_max = dsmax
    core.jumpf_(&ds_max, &ds)

    return ds

def knock():
    cdef:
        double de
        int icol

    core.knock_(&de, &icol)

    return de, icol


def knockf():
    cdef:
        double de
        int icol

    core.knockf_(&de, &icol)

    return de, icol


def get_seed():
    return rseed_.iseed1, rseed_.iseed2


cpdef set_seed(int a, int b):
    rseed_.iseed1 = a
    rseed_.iseed2 = b


def get_nsec():
    return core.secst_.nsec


def get_sec_par(int n):
    """lookup for the secondary particle stack
    """
    if n < 0:
        n = core.NMS + n

    if n >= core.NMS:
        raise IndexError("The secondary particle stack out of range")

    return [core.secst_.es[n],
            core.secst_.xs[n],
            core.secst_.ys[n],
            core.secst_.zs[n],
            core.secst_.us[n],
            core.secst_.vs[n],
            core.secst_.ws[n],
            core.secst_.kps[n],
            core.secst_.ibodys[n],
            core.secst_.ms[n]]


cpdef double rand():
    return core.rand_()
