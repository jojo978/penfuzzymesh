# distutils: language = c++
from libc.math cimport fabs

from . cimport core
from . cimport octree

from .core cimport (__track_mod_MOD_e as track_e,
                    __track_mod_MOD_x as track_x,
                    __track_mod_MOD_y as track_y,
                    __track_mod_MOD_z as track_z,
                    __track_mod_MOD_u as track_u,
                    __track_mod_MOD_v as track_v,
                    __track_mod_MOD_w as track_w,
                    __track_mod_MOD_wght as track_wght,
                    __track_mod_MOD_ibody as track_ibody,
                    __track_mod_MOD_mat as track_mat,
                    __track_mod_MOD_kpar as track_kpar,
                    __track_mod_MOD_ilb as track_ilb,
                    __track_mod_MOD_ipol as track_ipol,
                    __track_mod_MOD_mhinge as track_mhinge,
                    __track_mod_MOD_e0segm as track_e0segm,
                    __track_mod_MOD_desoft as track_desoft,
                    __track_mod_MOD_ssoft as track_ssoft)

from .core cimport __penelope_mod_MOD_eabs as penelope_eabs


DEF rev = 5.10998928e5
DEF trev = rev*2


cdef class Main:
    def __cinit__(self, octree.Octree t, *args, **kwars):
        self.t = t
        self.flight_logs.resize(core.NMS)

    cdef void init_primary_particle(self):
        pass

    cdef void start_of_track(self):
        pass

    cdef void after_step(self, double ds, double dsef):
        pass

    cdef void end_of_track(self):
        pass

    cdef void end_of_event(self):
        pass

    cdef bint cross_boundary(self):
        return False

    cdef void energy_deposited(self, double x, double y, double z, double de, int body):
        pass

    cdef void new_secondary_particle(self, int i):
        pass

    def run(self, int nevents):
        cdef:
            double ds, dsef, ds_max, de, last_e
            double last_x, last_y, last_z
            int icol, ncross, nsec, last_mat, i, i_secst
            int left
            octree.Octree oc

        global track_e, track_ibody, track_mat

        oc = self.t

        for i in range(nevents):
            core.cleans_()

            # this method is supposed to place (a) primary particle(s) into
            # the secondary particle stack and adjusting the flight logs accordingly
            self.init_primary_particle()

            # until all secondary particles are tracked.
            while True:
                core.secpar_(&left)

                if left < 1:
                    # notify user of the end of an event.
                    self.end_of_event()
                    break

                # when a secondary particle is popped from the stack,
                # deposited energy needs to be taken back.
                self.energy_deposited(track_x, track_y, track_z, -track_e * track_wght, track_ibody)

                # with an energy above its absorption energy, this particle will be tracked.
                # (track_x, track_y, track_z) must be inside the octree domain.
                oc.node = octree.find_node(oc.root, track_x, track_y, track_z)
                oc.body_inclusion_ = self.flight_logs[core.secst_.nsec]

                # notify user that a new track is about to start.
                self.start_of_track()

                core.start_()
                ds_max = oc.ds_maxs[track_ibody]

                # simulation interactions are to be simulated
                # until the particle lose energy below the absorption energy,
                # escape the domain, or user request the stop.
                while True:
                    # here track_e >= eabs
                    last_e = track_e
                    last_mat = track_mat
                    last_x, last_y, last_z = track_x, track_y, track_z

                    core.jumpf_(&ds_max, &ds)
                    ncross = oc.step_(&track_x, &track_y, &track_z, track_u, track_v, track_w,
                                      ds, &dsef, &track_ibody, &track_mat)

                    # notify user that a step is performed. (The particle is just moved.)
                    self.after_step(ds, dsef)

                    # check if material is changed by the last step_.
                    if ncross > 0 and last_mat != track_mat:
                        if track_mat == 0:
                            # it's escaping the domain
                            break

                        # soft interactions for electron and positron
                        if track_desoft > 0 and track_kpar != 2:
                            track_e = track_e0segm - track_ssoft * dsef

                            if track_e < penelope_eabs[track_mat - 1][track_kpar - 1]:
                                # the particle doesn't enough energy to reach the surface with energy above eabs.
                                if track_kpar == 3:
                                    # positron came to a halt and annihilates
                                    self.energy_deposited(last_x, last_y, last_z,
                                                          (last_e + trev) * track_wght, track_ibody)
                                    last_nsec = core.secst_.nsec
                                    core.panar_(&penelope_eabs[track_mat][2])
                                    # notify user of newly created secondary particles
                                    # from positron annihilation.
                                    for i_secst in range(last_nsec, core.secst_.nsec):
                                        # notify user of newly created secondary particles.
                                        self.new_secondary_particle(i_secst)
                                        self.flight_logs[i_secst] = oc.body_inclusion_
                                else:
                                    # electron came to a halt at (last_x, last_y, last_z).
                                    self.energy_deposited(last_x, last_y, last_z,
                                                          last_e * track_wght, track_ibody)
                                break

                        # material is just chagned and stopped at the boundary
                        # check if the particle reaches the detector body.
                        # user can decide if this particle should continue
                        # to be tracked or be discarded.
                        if self.cross_boundary():
                            break

                        # update the ds_max for the next step_.
                        ds_max = oc.ds_maxs[track_ibody]

                        core.start_()
                        continue

                    # the particle didn't cross a material changing boundary.
                    # but ibody might have been changed.
                    ds_max = oc.ds_maxs[track_ibody]
                    
                    last_nsec = core.secst_.nsec
                    core.knockf_(&de, &icol)

                    # any secondary particles are just created by the last interaction?
                    # if so, flight_logs needs to be updated for newly created particles.
                    if last_nsec != core.secst_.nsec:
                        for i_secst in range(last_nsec, core.secst_.nsec):
                            # notify user of newly created secondary particles.
                            # de includes all energies of newly created secondary particles.
                            self.new_secondary_particle(i_secst)
                            self.flight_logs[i_secst] = oc.body_inclusion_

                    # notify user the particle interaction (knockf_) is just finished.
                    # de amount of energy is deposited here.
                    # it may not be that all of de is deposited here if next jump escaped this material.
                    self.energy_deposited(track_x, track_y, track_z, de * track_wght, track_ibody)

                    # if the particle loses the energy below the absorption threshold,
                    # stop tracking this particle.
                    if track_e < penelope_eabs[track_mat - 1][track_kpar - 1]:
                        # all of track_e energy is assumed to be deposited here.
                        if track_kpar == 3:
                            # positron annihilation should happen here.
                            # panar_ is supposed to put a trev amount of energy into the secondary particle stack.
                            core.panar_(&penelope_eabs[track_mat - 1][2])
                            self.energy_deposited(track_x, track_y, track_z,
                                                  (track_e + trev) * track_wght, track_ibody)
                        else:
                            self.energy_deposited(track_x, track_y, track_z,
                                                  track_e * track_wght, track_ibody)
                        break

                # notify user that the track just ended.
                self.end_of_track()
