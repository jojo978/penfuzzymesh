import numpy as np

epsilon = 1.0e-10


def box_tri_mesh_(mins, maxs, tris, body, in_or_out):
    bounds = np.stack([mins, maxs]).astype(np.float64)
    eps = np.ones(bounds.shape[1], dtype=np.float64) * epsilon

    if in_or_out == 0:
        bounds[0, :] -= eps
        bounds[1, :] += eps

    rows = [
        [0, 0, 0],
        [1, 0, 0],
        [1, 1, 0],
        [0, 1, 0],
        [0, 0, 1],
        [1, 0, 1],
        [1, 1, 1],
        [0, 1, 1]
    ]

    cols = [[0, 1, 2]] * 8

    vertices = bounds[rows, cols]

    quads = [
        [3, 2, 1, 0],
        [4, 5, 6, 7],
        [0, 1, 5, 4],
        [1, 2, 6, 5],
        [2, 3, 7, 6],
        [3, 0, 4, 7]
    ]

    def quad_triangulations(a, b, c, d, t):
        if in_or_out == 0:
            t.extend([[vertices[a], vertices[b], vertices[c], body, in_or_out],
                      [vertices[a], vertices[c], vertices[d], body, in_or_out]])
        else:
            t.extend([[vertices[a], vertices[b], vertices[c], body, in_or_out],
                      [vertices[a], vertices[c], vertices[d], body, in_or_out]])

    for a, b, c, d in quads:
        quad_triangulations(a, b, c, d, tris)


def box_tri_mesh(mins, maxs, tris, body):
    for i in range(3):
        if mins[i] >= maxs[i]:
            raise ValueError("invalid cuboid extent")

    box_tri_mesh_(mins, maxs, tris, body, 0)
    box_tri_mesh_(mins, maxs, tris, body, 1)
