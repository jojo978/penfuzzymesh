from libcpp.vector cimport vector
from libcpp.set cimport set

from .octree cimport Octree


cdef class Main:
    cdef Octree t
    cdef vector[set[int]] flight_logs

    cdef void init_primary_particle(self)
    cdef void start_of_track(self)
    cdef void after_step(self, double ds, double dsef)
    cdef bint cross_boundary(self)
    cdef void energy_deposited(self, double x, double y, double z,
                               double de, int body)
    cdef void new_secondary_particle(self, int i_secst)
    cdef void end_of_track(self)
    cdef void end_of_event(self)
