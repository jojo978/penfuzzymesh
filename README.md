# penFuzzyMesh #

Python binding for some of PENELOPE and fuzzy triangular mesh geometric routine

### What is this repository for? ###

* CMake build file for a PENELOPE shared object (Though 2018 PENELOPE source codes are not bundled due to a license or copyright issue.)
* Python bindings for some of PENELOPE subroutines and module variables
    * This allows you easy access to PENELOPE in interactive sessions.
* Cython pxd file for some of PENELOPE subroutines and module variables
    * This allows you a faster access to PENELOPE in case you implement your own PENELOPE main function in Cython.
* You can add bindings to remaining PENELOPE features by modifying following files appropriately
    * external/penelope/include/penelope.h
    * src/penelope/core.pxd
* Fuzzy triangular mesh geometric routine. (It's typically called 'step' or something similar in PENELOPE documentation.)
    * implemented using the octree data structure
    * User needs to supply two copies of meshes for a single solid. One is slightly large and one is slightly small in the scale of floating point round error. (let say 1.0e-11 or similar.)

### How do I get set up? ###

* Summary of set up
    * Build PENELOPE shared object
        * Copy 2018 PENELOPE fortran source files to 'external/penelope/src'.
            * At line 130 of penelope.f, modify NMS from 1000, 10000. (I chose 10000 although 1000 is large enough for most cases.)
            * At line 198 of penelope.f, modify PMFILE length from 20 to 64. (20 file name length seems way too short.)
        * Make a directory 'external/penelope/build' and go this directory.
        * Execute 'cmake ..'
        * Execute 'cmake --build .'
    * Copy the PENELOPE shared object to an appropriate Python directory so that python interpreter find this shared object.
        For example, for Anaconda3 on Windows Subsystem for Linux 2, user: young, environment: igl, this directory is '/home/young/anaconda3/envs/igl/lib'
    * Build python binding for PENELOPE shared object
        * 'python setup.py install' or similar in the project directory.
* Dependencies
    * 2018 PENELOPE fortran source code
        * material.f
        * penelope.f
        * pengeom.f
        * penvared.f
        * rita.f
        * timer.f 
    * CMake
    * gfortran compiler (It's only fortan compiler tested)
    * Cython
    * NumPy

### Examples ###
* 500 um Nb strip structure for Raven project
    * Go to the example directory '/examples'
    * Execute 'python setup.py build_ext --inplace' or something similar
    * Start an IPython session
    * Execute '%run penelope_main.py'
* Phosphorous doped silicon target
    * Calculates the photon spectrum from 80 keV incident electron bmeas
    * 21*nm thick amorphous phosphorous and silicon mixture target
    * Saves the data into the hdf5 file 'psi.h5'
